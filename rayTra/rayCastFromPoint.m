% This is the main program for casting the ray and measure the returned
% distance. The ray constructed in this program is directly from the
% velodyne point measurement.

clear all;

addpath ~/Documents/new-20140617-22/velodyne_points/data;
listing=dir('~/Documents/new-20140617-22/velodyne_points/data');

%% Get some calibration matrices
M_v_c=getVelToCam;
M_R = M_v_c(1:3,1:3);
M_t = M_v_c(1:3,4);
Proj = [1054.2 0.00 769.264;
        0.00 1055.0 604.039;
        0.00 0.00 1.00];
origin = [0,0,0,1]';
origin = M_v_c * origin;

%% Ray-casting
i = 201;
filename = listing(i+2).name;
laser_point = load(filename);

% Getting the segment file and plane model file
segfile = '~/Documents/auto-eval/rayTra/test200/000200_seg.png';
planefile = '~/Documents/auto-eval/rayTra/test200/000200_seg_planes_new.txt';
segment = loadSegfile(segfile);
planes = parsePlanefile(planefile);

intersection_map = zeros(3,size(laser_point,1));
% loops for constructing all the rays in one velodyne revolution
    for p=1:size(laser_point,1)
        disp(p);
        x = laser_point(p,1);
        y = laser_point(p,2);
        z = laser_point(p,3);
        
        if (x==0 && y==0 && z==0)
            continue
        end

        s = origin(1:3);
        u = [x,y,z,1]';
        u = M_v_c * u;
        u = u(1:3);
        ray(1:3,1) = s;
        ray(1:3,2) = u;
        intersection = rayTracing(ray,segment,planes,Proj);
        intersection_map(:,p) = intersection;
    end
    
    
    intersection_map;
    
    
