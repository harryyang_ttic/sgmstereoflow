%%
% This matlab program evaluate the measurement error between 
% ray laser data (lasercam_xxxxxx.txt) and our ray casting model 
% (raytrace_xxxxx.txt). It measures the distance between 
% each corresponding points in the ray-direction.
% 
% Author: Zhixiang Wu, 07-22-2014

clear;

lasercam_raw = load('./bin/lasercam_000200.txt');
raytrace_raw = load('./bin/raytrace_000200.txt');

origin = [0.281037 -0.0888998 -0.694856];

lasercam = zeros(size(lasercam_raw));
lasercam_unit = zeros(size(lasercam_raw));
lasercam_len = zeros(size(lasercam_raw,1),1);
raytrace = zeros(size(raytrace_raw));
for i=1:size(lasercam,1)
    if lasercam_raw(i,1)~=0 && lasercam_raw(i,2)~=0 && lasercam_raw(i,3)~=0
        lasercam(i,:) = lasercam_raw(i,:) - origin;
        lasercam_len(i) = sqrt(sum(lasercam(i,:).^2));
        lasercam_unit(i,:) = lasercam(i,:) ./ sqrt(sum(lasercam(i,:).^2));
        raytrace(i,:) = raytrace_raw(i,:) - origin;
    end
end

diff = lasercam - raytrace;
diff2 = zeros(size(raytrace_raw,1),1);
total = 0;
rate2 = 0;
rate5 = 0;
rate75 = 0;
rate10 = 0;
for i=1:size(lasercam,1)
    if lasercam(i,1)~=0 && lasercam(i,2)~=0 && lasercam(i,3)~=0
        diff2(i) = diff(i,1) ./ lasercam_unit(i,1);
        if (abs(diff2(i)) > 5) % remove some outliners
            continue;
        end
        total = total + 1;
        if (abs(diff2(i)) / lasercam_len(i) < 0.02)
            rate2 = rate2 + 1;
        end
        if (abs(diff2(i)) / lasercam_len(i) < 0.05)
            rate5 = rate5 + 1;
        end
        if (abs(diff2(i)) / lasercam_len(i) < 0.075)
            rate75 = rate75 + 1;
        end
        if (abs(diff2(i)) / lasercam_len(i) < 0.1)
            rate10 = rate10 + 1;
        end
    end
end

%X = [-100,-80,-60,-40,-20,-10,-5,-3,-2,-1,0,1,2,3,5,10,20,40,60,80,100];
hist(abs(diff2(diff2~=0 & abs(diff2)<20)),50);
