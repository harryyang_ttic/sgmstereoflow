#include "Drawing.h"
#include <cmath>
#include <vector>
#include "Mathematics.h"
#include "Exception.h"

namespace rev {
    
// Prototype declaration
void drawPoint(const int x, const int y, const Color<unsigned char>& pointColor, Image<unsigned char>& image);
void drawHorizontalLine(const int startX, const int endX, const int y, const Color<unsigned char>& lineColor, Image<unsigned char>& image);
    
    
void drawLine(const int startX,
              const int startY,
              const int endX,
              const int endY,
              const Color<unsigned char>& lineColor,
              Image<unsigned char>& image)
{
    int distanceX = endX > startX ? endX - startX : startX - endX;
    int distanceY = endY > startY ? endY - startY : startY - endY;
    
    int stepX = endX > startX ? 1 : -1;
    int stepY = endY > startY ? 1 : -1;
    
    int drawX = startX;
    int drawY = startY;

    if (distanceX > distanceY) {
        int err = -distanceX;
        for (int i = 0; i <= distanceX; ++i) {
            drawPoint(drawX, drawY, lineColor, image);
            drawX += stepX;
            err += 2*distanceY;
            if (err >= 0) {
                drawY += stepY;
                err -= 2*distanceX;
            }
        }
    } else {
        int err = -distanceY;
        for (int i = 0; i <= distanceY; ++i) {
            drawPoint(drawX, drawY, lineColor, image);
            drawY += stepY;
            err += 2*distanceX;
            if (err >= 0) {
                drawX += stepX;
                err -= 2*distanceY;
            }
        }
    }
}
    
void drawCircle(const int centerX,
                const int centerY,
                const int radius,
                const Color<unsigned char>& lineColor,
                Image<unsigned char>& image)
{
    if (image.channelTotal() != 3) {
        throw Exception("rev::drawCircle", "image is not a color image");
    }
        
    int err = 0;
    int plus = 1;
    int minus = (radius << 1) - 1;
    int offsetX = radius;
    int offsetY = 0;
        
    while (offsetX >= offsetY) {
        int firstStartX = centerX - offsetX;
        int firstEndX = centerX + offsetX;
        int secondStartX = centerX - offsetY;
        int secondEndX = centerX + offsetY;
        int firstStartY = centerY - offsetY;
        int firstEndY = centerY + offsetY;
        int secondStartY = centerY - offsetX;
        int secondEndY = centerY + offsetX;
            
        // Draw horizontal lines
        drawHorizontalLine(firstStartX, firstEndX, firstStartY, lineColor, image);
        drawHorizontalLine(firstStartX, firstEndX, firstEndY, lineColor, image);
        drawHorizontalLine(secondStartX, secondEndX, secondStartY, lineColor, image);
        drawHorizontalLine(secondStartX, secondEndX, secondEndY, lineColor, image);
            
        // Update offsets
        ++offsetY;
        err += plus;
        plus += 2;
        int maskValue = (err <= 0) - 1;
        err -= minus & maskValue;
        offsetX += maskValue;
        minus -= maskValue & 2;
    }
}
    
Image<unsigned char> drawSegmentBoundary(const Image<unsigned char>& originalImage,
                                         const Image<unsigned short>& segmentImage,
                                         const Color<unsigned char>& boundaryColor)
{
    // Pixel offsets of 8-neighbors
    const int eightNeighborTotal = 8;
    const int eightNeighborOffsetX[8] = {-1,-1, 0, 1, 1, 1, 0,-1};
    const int eightNeighborOffsetY[8] = { 0,-1,-1,-1, 0, 1, 1, 1};
        
    // Check image size
    int width = originalImage.width();
    int height = originalImage.height();
    if (segmentImage.width() != width || segmentImage.height() != height) {
        throw Exception("rev::drawSegmentBoundary", "image sizes are different");
    }
        
    // Copy input image
    Image<unsigned char> boundaryImage = convertToColor(originalImage);
        
    // Draw boundary
    std::vector<bool> boundaryFlags(width*height, false);
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            int pixelLabelIndex = segmentImage(x, y);
                
            bool drawBoundary = false;
            for (int neighborIndex = 0; neighborIndex < eightNeighborTotal; ++neighborIndex) {
                int neighborX = x + eightNeighborOffsetX[neighborIndex];
                int neighborY = y + eightNeighborOffsetY[neighborIndex];
                if (neighborX < 0 || neighborX >= width || neighborY < 0 || neighborY >= height) continue;
                    
                int neighborPixelIndex = width*neighborY + neighborX;
                if (boundaryFlags[neighborPixelIndex]) continue;
                if (segmentImage(neighborX, neighborY) != pixelLabelIndex) {
                    drawBoundary = true;
                    break;
                }
            }
                
            if (drawBoundary) {
                boundaryFlags[width*y + x] = true;
                boundaryImage.setColor(x, y, boundaryColor);
            }
        }
    }
        
    return boundaryImage;
}
    
Image<unsigned char> drawKeypoint(const Image<unsigned char>& originalImage,
                                  const std::vector<Keypoint>& keypoints,
                                  const Color<unsigned char>& lineColor)
{
    const double drawingScale = 5.0;
    const double drawingScaleArrow = 0.75;
    
    Image<unsigned char> keypointImage = convertToColor(originalImage);
        
    int keypointTotal = static_cast<int>(keypoints.size());
    for (int keypointIndex = 0; keypointIndex < keypointTotal; ++keypointIndex) {
        double x = keypoints[keypointIndex].x();
        double y = keypoints[keypointIndex].y();
        double scale = keypoints[keypointIndex].scale();
        double orientation = keypoints[keypointIndex].orientation();
        orientation *= -1.0;
            
        int startX = static_cast<int>(x + 0.5);
        int startY = static_cast<int>(y + 0.5);
        double lineLength = scale*drawingScale;
        double arrowLength = scale*drawingScaleArrow;
        double diffLength = lineLength - arrowLength;
        int endX = static_cast<int>(lineLength*cos(orientation) + 0.5) + startX;
        int endY = static_cast<int>(lineLength*(-sin(orientation)) + 0.5) + startY;
        int endArrow1X = static_cast<int>(diffLength*cos(orientation + REV_PI/18.0) + 0.5) + startX;
        int endArrow1Y = static_cast<int>(diffLength*(-sin(orientation + REV_PI/18.0)) + 0.5) + startY;
        int endArrow2X = static_cast<int>(diffLength*cos(orientation - REV_PI/18.0) + 0.5) + startX;
        int endArrow2Y = static_cast<int>(diffLength*(-sin(orientation - REV_PI/18.0)) + 0.5) + startY;
            
        drawLine(startX, startY, endX, endY, lineColor, keypointImage);
        drawLine(endX, endY, endArrow1X, endArrow1Y, lineColor, keypointImage);
        drawLine(endX, endY, endArrow2X, endArrow2Y, lineColor, keypointImage);
    }
    
    return keypointImage;
}
    

void drawPoint(const int x, const int y, const Color<unsigned char>& pointColor, Image<unsigned char>& image) {
    if (x < 0 || x >= image.width() || y < 0 || y >= image.height()) return;
        
    image.setColor(x, y, pointColor);
}
    
void drawHorizontalLine(const int startX, const int endX, const int y, const Color<unsigned char>& lineColor, Image<unsigned char>& image) {
    if (y < 0 || y >= image.height()) return;;
        
    for (int pointX = startX; pointX <= endX; ++pointX) {
        drawPoint(pointX, y, lineColor, image);
    }
}
    
}
