//#include <boost/numeric/ublas/vector.hpp>
//#include <boost/numeric/ublas/matrix.hpp>
#include <eigen3/Eigen/Dense>
#include <nlopt.hpp>

#include <omp.h>
#include "dirent.h"
#include "string.h"
#include <stdlib.h> // atoi
#include <float.h>
#include <limits.h>
#include <queue>
#include <fstream>
#include <iostream>
#include <revlib.h>

#define EPS 1e-6

//using namespace boost::numeric::ublas;
using namespace Eigen;

//std::vector<std::vector<vector<float> > > findSegment(matrix<float> &seg);
//int findNearestSegment(vector<float> &uv, std::vector<std::vector<vector<float> > > &segment, int segId);

std::vector<VectorXf> computeBoundingBox(MatrixXf &seg);
int findNearestSegment_approx(Vector2i &uv, std::vector<VectorXf> &bb, int segId);

//int findNearestSegment(vector<float> &uv, matrix<float> &seg, int segId, int *distmap, int thres);
//int findNearest(std::queue<std::pair<int, int> > &queue, int i, matrix<float> &seg, int *distmap, int m, int n, int thres);

std::vector<VectorXf> parseVelodynefile(const char *Velodynefile);
std::vector<VectorXf> parsePlanefile(const char *planefile);
MatrixXf loadSegfile(const char *segfile);


int main(int argc, char *argv[]) {

    if (argc != 5) {
        std::cout << "Usage: ./castRay Velo_dir segfile_dir planefile_dir CalibMatrix.txt" << std::endl;
        return -1;
    }
    const char *path = argv[1];
    const char *segpath = argv[2];
    const char *planepath = argv[3];
	const char *calibfilename = argv[4];

	omp_set_num_threads(omp_get_max_threads());
 
	Matrix4f M_v_c;
	M_v_c << 0, -1 ,0, 0,
			 0,0,-1,0,
		       1,0,0,0,
				0,0,0,1;

    std::ifstream calibfile(calibfilename);
    if (!calibfile.is_open()) {
        std::cout << "Open file error: " << calibfilename << std::endl;
		return -1;
    } else {
		int i=0, j=0;
    	while (!calibfile.eof() && i<4) {
			calibfile >> M_v_c(i,j);
			if (++j>3) {
				j=0;
				i++;
			}
    	}
	}
 
    Matrix3f M_Proj;
    M_Proj(0, 0) = 1019.305;
    M_Proj(0, 1) = 0.0;
    M_Proj(0, 2) = 789.380;
    M_Proj(1, 0) = 0.0;
    M_Proj(1, 1) = 1019.305;
    M_Proj(1, 2) = 612.212;
    M_Proj(2, 0) = 0.0;
    M_Proj(2, 1) = 0.0;
    M_Proj(2, 2) = 1.0;

    /*matrix<float> R_rect(3,3);
    R_rect(0,0) = 9.999239e-01; 
    R_rect(0,1) = 9.837760e-03;
    R_rect(0,2) = -7.445048e-03;
    R_rect(1,0) = -9.869795e-03;
    R_rect(1,1) = 9.999421e-01;
    R_rect(1,2) = -4.278459e-03;
    R_rect(2,0) = 7.402527e-03;
    R_rect(2,1) = 4.351614e-03;
    R_rect(2,2) = 9.999631e-01;*/

    VectorXf origin(4);
    int thres = 0;

    std::cout << "Opening path: " << path << std::endl;
    DIR* VelodyneDir = opendir(path);
    if (VelodyneDir) {
        struct dirent* vFile;
        int VelInd = 0;
		std::vector<std::vector<VectorXf> > laser_points_all;
		std::vector<MatrixXf> segments_all;
		std::vector<std::vector<VectorXf> > planes_all;
		std::vector<std::vector<VectorXf> > bb_all;

		int m=0, n=0;
        while ((vFile = readdir(VelodyneDir)) != NULL) {
            if (strcmp(vFile->d_name, ".") == 0) continue;
            if (strcmp(vFile->d_name, "..") == 0) continue;

            //if (strcmp(vFile->d_name, "000200.txt") != 0) continue;
            VelInd++;
            std::cout << "Opening laser file: " << vFile->d_name << std::endl;
            char *fullpath = (char*) malloc(strlen(path) + strlen(vFile->d_name) + 2);
            fullpath[0] = '\0';
            strcat(fullpath, path);
            strcat(fullpath, vFile->d_name);
            std::vector<VectorXf> laser_point = parseVelodynefile(fullpath);
			laser_points_all.push_back(laser_point);
            std::cout << "parse velodyne file finished." << std::endl;
			free(fullpath);

			fullpath = (char*) malloc(strlen(segpath) + strlen(vFile->d_name) + 6);
			fullpath[0] = '\0';
            strcat(fullpath, segpath);
            strcat(fullpath, vFile->d_name);
			fullpath[strlen(fullpath)-4] = '\0';
			strcat(fullpath, "_seg.png");
            MatrixXf segment = loadSegfile(fullpath);
			segments_all.push_back(segment);
            std::cout << "load seg file finished." << std::endl;
			free(fullpath);

			fullpath = (char*) malloc(strlen(planepath) + strlen(vFile->d_name) + 13);
			fullpath[0] = '\0';
            strcat(fullpath, planepath);
            strcat(fullpath, vFile->d_name);
			fullpath[strlen(fullpath)-4] = '\0';
			strcat(fullpath, "_seg_planes.txt");
            std::vector<VectorXf> planes = parsePlanefile(fullpath);
			planes_all.push_back(planes);
            std::cout << "parse plane file finished." << std::endl;
			free(fullpath);

            if (m==0 && n==0){
				m = segment.rows();
            	n = segment.cols();
			}
			else
				if (m!=segment.rows() || n!=segment.cols()) {
					std::cout << "error: image size does not match!" << std::endl;
				}
            std::vector<VectorXf> bb = computeBoundingBox(segment);
			bb_all.push_back(bb);
		} // end of scanning velodyne folder
        closedir(VelodyneDir);

		unsigned int total_points = 0;		
		for (unsigned i=0; i<laser_points_all.size(); i++)
			total_points += laser_points_all[i].size();

        std::vector<Vector3f> intersections;
        std::vector<Vector3f> laser_in_camera;
        std::vector<std::pair<int,int> > iplane; // saving the computed intersection plane id for each laser beam

			intersections.clear();
			laser_in_camera.clear();
			iplane.clear();

            origin(0) = 0;
            origin(1) = 0;
            origin(2) = 0;
            origin(3) = 1;
            origin = M_v_c * origin;
            Vector3f ray_ori;
            ray_ori(0) = origin(0);
            ray_ori(1) = origin(1);
            ray_ori(2) = origin(2);

            std::cout << "ray origin: " << ray_ori(0) << " " << ray_ori(1) << " " << ray_ori(2) << std::endl;

#pragma omp parallel for
			for (unsigned vel=0; vel<laser_points_all.size(); vel++) {
		
                for (int p = 0; p < laser_points_all[vel].size(); p++) {
                    if (p % 10000 == 0) std::cout << "." << std::flush;

                    Vector3f intersection;
                    intersection(0) = 0;
                    intersection(1) = 0;
                    intersection(2) = 0;
					int inter_plane = -1;
                    VectorXf point = laser_points_all[vel][p];
                    if (point(0) == 0 && point(1) == 0 && point(2) == 0) { // discard the zero laser point
                        continue;
                    }
                    VectorXf X(4);
                    X(0) = point(0);
                    X(1) = point(1);
                    X(2) = point(2);
                    X(3) = 1;
                    X = M_v_c * X;
                    Vector3f ray_unit;
                    ray_unit(0) = X(0);
                    ray_unit(1) = X(1);
                    ray_unit(2) = X(2);

                    // Discard the laser point if it is projected outside the camera frame
                    Vector3f UV = M_Proj * ray_unit;
                    float u = UV(0) / UV(2), v = UV(1) / UV(2);
                    if (u < 0 || round(u) >= m || v < 0 || round(v) >= n || ray_unit(2) <= 0) {
                        continue;
                    }

                    //ray_unit = prod(R_rect, ray_unit);
					Vector3f lasercam = ray_unit;
                    //laser_in_camera.push_back(ray_unit);
                    ray_unit = ray_unit - ray_ori;
                    ray_unit = ray_unit / ray_unit.norm(); // normalize the ray unit vector

                    float mindist = FLT_MAX;
                    int minu = 0, minv = 0;
                    for (int i = 0; i < planes_all[vel].size(); i++) {
                        double u, v, s;

                        /* Finding intersection of ray and a plane */
                        VectorXf plane = planes_all[vel][i];
                        if (plane[4] == 0) continue; // The plane is marked as in-valid
                        Vector3f plane_coef;
                        plane_coef(0) = plane(0);
                        plane_coef(1) = plane(1);
                        plane_coef(2) = plane(2);
                        double coef = plane_coef.transpose() * ray_unit;
                        if (std::abs(coef) > EPS) {
                            s = (plane(3) + plane_coef.transpose() * ray_ori) / -coef;
                            Vector3f X_in = ray_ori + s * ray_unit;
                            //if (X_in(2) < 0) { // ray-plane intersection fall at the back of the camera
                            //	u = -1;
                            //	v = -1;
                            //	s = -1;
                            //}
                            //else {
                            Vector3f U_in = M_Proj * X_in;
                            //U_in = prod(M_Proj, U_in);
                            u = U_in(0) / U_in(2);
                            v = U_in(1) / U_in(2);
                            //}
                        } else {
                            u = -1;
                            v = -1;
                            s = -1;
                        }

                        /* candidate-polling algorithm */
                        if (s < 0 || u < 0 || round(u) >= m || v < 0 || round(v) >= n)
                            continue;
                        if (segments_all[vel](round(u), round(v)) == plane[5]) // Case 1: Intersection point fall into the same segment region
                        {
                            if (s < mindist && s > 0) { // finding the intersection with the minimal distance
                                minu = u;
                                minv = v;
                                mindist = s;
                                intersection = ray_ori + s * ray_unit;
                                inter_plane = plane[5];
                            }
                        } else {
                            Vector2i uv;
                            uv(0) = round(u);
                            uv(1) = round(v);
                            //int dis = findNearestSegment(uv, segment, i, distmap, thres); // The BFS method - computationally expensive
                            int dis = findNearestSegment_approx(uv, bb_all[vel], plane[5]); // An Bounding-box approximate method                     
                            if (dis > thres)
                                continue;
                            else // Case 2: Intersection point fall within a certain distance to the segment region
                            {
                                if (s < mindist && s > 0) { // finding the intersection with the minimal distance
									minu = u;
									minv = v;
                                    mindist = s;
                                    intersection = ray_ori + s * ray_unit;
                                    inter_plane = plane[5];
                                }
                            }
                        }

                    } // end of loop for planes
					
					#pragma omp critical
					{
						laser_in_camera.push_back(lasercam);
                    	intersections.push_back(intersection);
						iplane.push_back(std::pair<int,int>(vel,inter_plane));
					}
                    //if (intersection(0) == 0 && intersection(1) == 0 && intersection(2) == 0){
                    //	laser_in_camera[p] = intersection;
                    //}

                } // end of loop for laser points
			} // end of loop for different files

                float totalDist = 0;
                int totalValid = 0;
                for (unsigned int i = 0; i < intersections.size(); i++) {
                    if (!(intersections[i](0) == 0 && intersections[i](1) == 0 && intersections[i](2) == 0)) {
                        totalDist += (laser_in_camera[i] - intersections[i]).norm();
                        totalValid++;
                    }
                }
                float avgDist = totalDist / totalValid;
                std::cout << "\navg dist: " << avgDist << "\ntotal valid point: " << totalValid << std::endl;
                totalValid = 0;
                for (unsigned int i = 0; i < intersections.size(); i++) {
                    if (!(intersections[i](0) == 0 && intersections[i](1) == 0 && intersections[i](2) == 0)) {
                        if ((laser_in_camera[i] - intersections[i]).norm() < 0.5*avgDist) {
                            totalValid++;
                        }
                    }
                }
                std::cout << "small error points:" << totalValid << std::endl;

            /* Output the ray-tracing result */
            char *outputfilename = (char*) malloc(sizeof(char)*20);
            outputfilename[0] = '\0';
            strcat(outputfilename, "raytrace_db.txt\0");
            //strcat(outputfilename, vFile->d_name);
            std::ofstream outputfile(outputfilename);
            for (unsigned i = 0; i < intersections.size(); i++) {
                outputfile << intersections[i](0) << " " << intersections[i](1) << " " << intersections[i](2) << std::endl;
            }
            outputfile.close();
            outputfilename[0] = '\0';
            strcat(outputfilename, "lasercam_db.txt\0");
            //strcat(outputfilename, vFile->d_name);
            outputfile.open(outputfilename);
            for (unsigned i = 0; i < laser_in_camera.size(); i++) {
                outputfile << laser_in_camera[i](0) << " " << laser_in_camera[i](1) << " " << laser_in_camera[i](2) << std::endl;
            }
            outputfile.close();
            free(outputfilename);
			

    } else
        std::cout << "Error open the Velodyne folder!" << std::endl;

    return 0;

}

/*
int findNearestSegment(vector<float> &uv, std::vector<std::vector<vector<float> > >&segment, int segId) {
        float mindist = FLT_MAX;
        for (unsigned i=0; i<segment[segId].size(); i++) {
                float dist = norm_2(segment[segId][i] - uv);
                if (dist < mindist) mindist = dist;
        }
        return mindist;
}*/

/*
std::vector<std::vector<vector<float> > > findSegment(matrix<float> &seg) {
        int maxR = 0;
    for (unsigned int u=0; u<seg.size1(); u++)
        for (unsigned int v=0; v<seg.size2(); v++)
            maxR = maxR > seg(u,v)? maxR:seg(u,v);

    std::vector<std::vector<vector<float> > > segment(maxR+1);
    for (unsigned int u=0; u<seg.size1(); u++) 
        for (unsigned int v=0; v<seg.size2(); v++) {
                        vector<float> pix(2);
                        pix(0) = u, pix(1) = v;
            int segId = seg(u,v);
                        segment[segId].push_back(pix);
        }
    
    return segment;

}
 */

int findNearestSegment_approx(Vector2i &uv, std::vector<VectorXf> &bb, int segId) {

    int u = uv(0), v = uv(1);
    float umin = bb[segId](0), umax = bb[segId](1);
    float vmin = bb[segId](2), vmax = bb[segId](3);
    // Case 1: fall into the bounding box
    if (u >= umin && u <= umax && v >= vmin && v <= vmax)
        return 0;
    // Case 2: distance to the bounding box
    // Divide the space into 8 regions as shown below:
    //   1 |    2    |  3
    //  ---|---------|-----    vmax
    //   4 | segment |  5
    //  ---|---------|-----    vmin
    //   6 |    7    |  8
    //    umin     umax

    if (u < umin && v > vmax) // in region 1
        return std::sqrt((u - umin)*(u - umin)+(v - vmax)*(v - vmax));
    if (u > umax && v > vmax) // in region 3
        return std::sqrt((u - umax)*(u - umax)+(v - vmax)*(v - vmax));
    if (v > vmax) // in region 2
        return v - vmax;
    if (u < umin && v < vmin) // in region 6
        return std::sqrt((u - umin)*(u - umin)+(v - vmin)*(v - vmin));
    if (u < umin) // in region 4
        return umin - u;
    if (u > umax && v < vmin) // in region 8
        return std::sqrt((u - umax)*(u - umax)+(v - vmin)*(v - vmin));
    if (v < vmin) // in region 7
        return vmin - v;
    if (u > umax) // in region 5
        return u - umax;

    return INT_MAX;
}

std::vector<VectorXf> computeBoundingBox(MatrixXf &seg) {

    int maxR = 0;
    for (unsigned int u = 0; u < seg.rows(); u++)
        for (unsigned int v = 0; v < seg.cols(); v++)
            maxR = maxR > seg(u, v) ? maxR : seg(u, v);
    std::cout << "maxR: " << maxR << std::endl;
    std::vector<VectorXf> bb(maxR + 1);
    int m = seg.rows();
    int n = seg.cols();
    for (int i = 0; i < maxR + 1; i++) {
        VectorXf newbb(4);
        newbb(0) = m;
        newbb(1) = 0;
        newbb(2) = n;
        newbb(3) = 0;
        bb[i] = newbb;
    }
    for (unsigned int u = 0; u < seg.rows(); u++) {
        for (unsigned int v = 0; v < seg.cols(); v++) {
            int segId = seg(u, v);
            bb[segId](0) = bb[segId](0) <= u ? bb[segId](0) : u;
            bb[segId](1) = bb[segId](1) >= u ? bb[segId](1) : u;
            bb[segId](2) = bb[segId](2) <= v ? bb[segId](2) : v;
            bb[segId](3) = bb[segId](3) >= v ? bb[segId](3) : v;
        }
    }

    return bb;
}

/*
int findNearestSegment(vector<float> &uv, matrix<float> &seg, int segId, int *distmap, int thres) {
    int m = seg.size1();
    int n = seg.size2();

    int v = uv(0), u = uv(1);

    //int *distmap = (int*) malloc(sizeof (int)*m * n);
    memset(distmap, 0, sizeof (int)*m * n);
    //for (int i = 0; i < m * n; i++)
    //    distmap[i] = -1;
    distmap[(u - 1) * n + v - 1] = 0;
    std::queue<std::pair<int, int> > q;
    q.push(std::pair<int, int>(u - 1, v - 1));
    int dist = findNearest(q, segId, seg, distmap, m, n, thres);

    //free(distmap);
    return dist;
}

int findNearest(std::queue<std::pair<int, int> > &queue, int i, matrix<float> &seg, int *distmap, int m, int n, int thres) {

    int dist = INT_MAX;
    while (!queue.empty()) {
        std::pair<int, int> cur = queue.front();
        queue.pop();
        int u = cur.first, v = cur.second;
        if (distmap[u*n+v] == thres)
            return INT_MAX;
        
        if (u + 1 < m && distmap[(u + 1) * n + v] == 0) {
            distmap[(u + 1) * n + v] = distmap[u * n + v] + 1;
            if (seg(u + 1, v) == i) {
                dist = distmap[(u + 1) * n + v];
                break;
            }
            queue.push(std::pair<int, int>(u + 1, v));
        }
        if (u - 1 >= 0 && distmap[(u - 1) * n + v] == 0) {
            distmap[(u - 1) * n + v] = distmap[u * n + v] + 1;
            if (seg(u - 1, v) == i) {
                dist = distmap[(u - 1) * n + v];
                break;
            }
            queue.push(std::pair<int, int>(u - 1, v));
        }
        if (v + 1 < n && distmap[u * n + v + 1] == 0) {
            distmap[u * n + v + 1] = distmap[u * n + v] + 1;
            if (seg(u, v + 1) == i) {
                dist = distmap[u * n + v + 1];
                break;
            }
            queue.push(std::pair<int, int>(u, v + 1));
        }
        if (v - 1 >= 0 && distmap[u * n + v - 1] == 0) {
            distmap[u * n + v - 1] = distmap[u * n + v] + 1;
            if (seg(u, v - 1) == i) {
                dist = distmap[u * n + v - 1];
                break;
            }
            queue.push(std::pair<int, int>(u, v - 1));
        }
        if (u + 1 < m && v + 1 < n && distmap[(u + 1) * n + v + 1] == 0) {
            distmap[(u + 1) * n + v + 1] = distmap[u * n + v] + 1;
            if (seg(u + 1, v + 1) == i) {
                dist = distmap[(u + 1) * n + v + 1];
                break;
            }
            queue.push(std::pair<int, int>(u + 1, v + 1));
        }
        if (u + 1 < m && v - 1 >= 0 && distmap[(u + 1) * n + v - 1] == 0) {
            distmap[(u + 1) * n + v - 1] = distmap[u * n + v] + 1;
            if (seg(u + 1, v - 1) == i) {
                dist = distmap[(u + 1) * n + v - 1];
                break;
            }
            queue.push(std::pair<int, int>(u + 1, v - 1));
        }
        if (u - 1 >= 0 && v + 1 < n && distmap[(u - 1) * n + v + 1] == 0) {
            distmap[(u - 1) * n + v + 1] = distmap[u * n + v] + 1;
            if (seg(u - 1, v + 1) == i) {
                dist = distmap[(u - 1) * n + v + 1];
                break;
            }
            queue.push(std::pair<int, int>(u - 1, v + 1));
        }
        if (u - 1 >= 0 && v - 1 >= 0 && distmap[(u - 1) * n + v - 1] == 0) {
            distmap[(u - 1) * n + v - 1] = distmap[u * n + v] + 1;
            if (seg(u - 1, v - 1) == i) {
                dist = distmap[(u - 1) * n + v - 1];
                break;
            }
            queue.push(std::pair<int, int>(u - 1, v - 1));
        }
    }

    return dist;
}
 */

MatrixXf loadSegfile(const char *segfile) {

    rev::Image<unsigned short> segImage;
    segImage = rev::read16bitImageFile(segfile);

    MatrixXf segment(segImage.width(), segImage.height());
    for (int u = 0; u < segImage.width(); u++)
        for (int v = 0; v < segImage.height(); v++)
            segment(u, v) = segImage(u, v);

    return segment;

}

std::vector<VectorXf> parsePlanefile(const char *planefile) {

    std::vector<VectorXf> planes;
    std::ifstream input(planefile);
    if (!input.is_open()) {
        std::cout << "Open file error: " << planefile << std::endl;
        return planes;
    }
    float tmp, no;
    char c;
    while (!input.eof()) {
        VectorXf plane(6);
        input >> no;
        input >> tmp;
        input >> tmp;
        input >> tmp;
        input >> tmp;
        input >> plane(0);
        input >> plane(1);
        input >> plane(2);
        input >> plane(3);
        input >> c;
        if (c == 'Y')
            plane(4) = 1;
        else
            plane(4) = 0;
		float scale = std::sqrt(plane(0)*plane(0)+plane(1)*plane(1)+plane(2)*plane(2));
		plane = plane / scale;
        if (c == 'Y')
            plane(4) = 1;
        else
            plane(4) = 0;
		plane(5) = no;
        planes.push_back(plane);
    }
    return planes;
}

std::vector<VectorXf> parseVelodynefile(const char *Velodynefile) {

    std::vector<VectorXf> laser_points;
    std::ifstream input(Velodynefile);
    if (!input.is_open()) {
        std::cout << "Open file error: " << Velodynefile << std::endl;
        return laser_points;
    }

    float tmp;
    while (!input.eof()) {
        VectorXf point(6);
        input >> tmp;
        point(0) = tmp / 100.0;
        input >> tmp;
        point(1) = tmp / 100.0;
        input >> tmp;
        point(2) = tmp / 100.0;
        input >> tmp;
        point(3) = tmp;
        input >> tmp;
        point(4) = tmp;
        input >> tmp;
        point(5) = tmp;
        laser_points.push_back(point);
    }
    return laser_points;
}


