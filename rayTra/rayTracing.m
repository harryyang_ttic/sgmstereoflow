function intersection = rayTracing(ray,segment,planes,Proj)
% Input:
%      ray - [x0,xt; y0,yt; z0,zt]
%      segfile - filename of the segment image
%      planefile - filename of the 3d plane models
%      Proj - 3*3 Camera Projection matrix (intrinsic)
% Output:
%      intersection - intersection point detected

thres = 50;

[m,n] = size(segment);

candidate = zeros(size(planes,1),1);
ray(:,2) = ray(:,2) / norm(ray(:,2)); % normalize the ray unit vector
%ind = 1;
parfor i=1:size(planes,1)
    [u,v,s] = findIntersection(ray, planes(i,:), Proj);
    u = round(u);
    v = round(v);
    if (s<=0)
        continue;
    end
    if (u<=0 || u>m || isnan(u))
        continue;
    end
    if (v<=0 || v>n || isnan(v))
        continue;
    end
    if (segment(u,v)==i)
        candidate(i)=i;
        %ind = ind + 1;
    else
        U = [u,v];
        dis = findNearestSegmentMex(U,double(segment),i);
        if (dis>thres)
            continue;
        else
            candidate(i)=i;
            %ind = ind + 1;
        end
    end
end

mindist = realmax;
intersection = [0,0,0]';
for i=1:size(planes,1)
    if (candidate(i)==0)
        continue;
    end
    [~,~,s] = findIntersection(ray, planes(i,:), Proj);
    if (s<mindist && s>0)
        mindist = s;
        intersection = ray(1:3,1) + s * ray(1:3,2);
    end
end

end
