function M_v_to_c = getVelToCam()
% This function returns the velodyne to camera transformation matrix
%
% Zhixiang Wu
% Toyota Technological Institute Chicago
% 07-17-2014


% Projection matrix P = K*R*T 
<<<<<<< HEAD
P = [786.698 -1037.57 -6.99006 -561.964;
611.421 -5.37066 -1039.72 -677.066;
0.999923 -0.00878322 -0.00878356 -1.10728];

[K,R,C,pp,pv] = decomposecamera(P);
C(2) = 0.24;
M_cam_to_a = [R,-R*C];
M_cam_to_a(4,4) = 1;
M_a_to_cam = (M_cam_to_a);
=======
P = [785.76 -1038.29 5.46148 -633.268;
	611.294 -18.4835 -1039.64 -872.053;
	0.999913 -0.00979284 -0.00878292 -1.06477];

[K,R,C,pp,pv] = decomposecamera(P);
M_a_to_cam = [R,C];
M_a_to_cam(4,4) = 1;
>>>>>>> d2892772e74fdbf1dfd48c85e09b8e2e2a54fb29

% transformation matrix from applanix(GPS) to velodyne
M_vel_wr_a = [1.00000 0.00070 -0.00177 0.41351;
            -0.00069 1.00000 0.00239 0.02507;
             0.00178 -0.00239 1.00000 0.09500;
             0 0 0 1];
<<<<<<< HEAD
% M_v_to_a = inv(M_a_to_v);

M_v_to_c = M_a_to_cam * M_vel_wr_a;
=======
M_v_to_a = inv(M_a_to_v);

M_v_to_c = M_v_to_a * M_a_to_cam;
>>>>>>> d2892772e74fdbf1dfd48c85e09b8e2e2a54fb29


%% velodyne to camera transformation provided by the KITTI dataset (can be used for reference)
% calib_velo_to_cam.txt
% R: 7.533745e-03 -9.999714e-01 -6.166020e-04 1.480249e-02 7.280733e-04 -9.998902e-01 9.998621e-01 7.523790e-03 1.480755e-02
% T: -4.069766e-03 -7.631618e-02 -2.717806e-01

% M = [7.533745e-03 -9.999714e-01 -6.166020e-04 -4.069766e-03;
%    1.480249e-02 7.280733e-04 -9.998902e-01 -7.631618e-02;
%    9.998621e-01 7.523790e-03 1.480755e-02 -2.717806e-01;
%    0 0 0 1];

end
