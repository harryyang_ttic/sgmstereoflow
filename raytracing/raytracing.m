% This piece of code runs over
% all the Data points, extracts the Laser
% ID and time stamp. It then calculates
% the exact ray emitted by the LIDAR
% using the Laser ID and time as the input
%
% This can be thought to be a simulation of the LIDAR
% and tries to replicate
% as it would work in actual scenario.

% Anish Acharya
% Toyota Technological Institute Chicago
% 07-16-2014


clear all;

%% Initiate all the global variables %%

N=64;                                                                                                   %Number of Lasers
n=10;                                                                                                   %frequency of rotation
phi=26.8*pi/180;                                                                                        %Total Vertical Field of Vision of the LIDAR
Rotation=[786.544 -1037.28 29.8578; 642.897 -17.9741 -1020.41; 0.999725 -0.00851384 0.021873];          %LIDAR to Camera Rotation
Translation=[ -517.225 -925.663 -1.05157];                                                              %LIDAR to Camera Translation


%%============================================================================
%% Load The Data %%
cd('/home-nfs/aacharya/Desktop/AUTO_EVAL/Michigan/velodyne_points');
Time_Start=textread('timestamps_start.txt');
Time_End=textread('timestamps_end.txt');
Time_Camera=textread('timestamps.txt');

listing=dir('/home-nfs/aacharya/Desktop/AUTO_EVAL/Michigan/velodyne_points/data');
cd('/home-nfs/aacharya/Desktop/AUTO_EVAL/Michigan/velodyne_points/data');

for i=1:numel(Time_Camera)
    i
    m=i+2;
    x=listing(m).name;
    [X1 Y1 Z1 I1 k block]=textread(x);
    
    t_start=Time_Camera(i,1)-((1/12)*(Time_End(i)-Time_Start(i)));                                           % Chunk of time stamp corresponding to Camera FOV
    t_end=Time_Camera(i,1)+((1/12)*(Time_End(i)-Time_Start(i)));
    
    delta_t=(Time_End(i)-Time_Start(i))/numel(X1);                                                           % Time difference between two consecutive laser triggering
    
    j_initial=ceil((t_start-Time_Start(i))/delta_t);                                                         % choosing Corresponding data points that fall in the FOV of Camera
    j_final=ceil((t_end-Time_Start(i))/delta_t);
    
    j_final-j_initial
    
    p=1;
    t=t_start;
    for j=j_start:1:j_final
        theta=2*pi*n*t;                                                         % the angular displacement due to LIDAR rotation
        
        
        
        u=[0 sin(phi*k(j)/N) cos(phi*k(j)/N)]*[cos(theta) 0 sin(theta);0 1 0;-sin(theta) 0 cos(theta)]; % unit vector w.r.t. LIDAR co-ordinate
        
        R(:,p,i)=Translation+u*Rotation;
        
        t=t+delta_t;
        p=p+1;
    end
    
end






%data=size()
%for i=1:data



%%============================================================================
%% Read the Laser index and Time Stamp %%

%t=......  ;                                                                              % to be extracted from data -time stamp
%k=......  ;                                                                              % to be extracted from the data- Laser Number
% j=1;
% for t=0:0.5:5
%     for k=1:64
%         theta=2*pi*n*t;                                                                           % the angular displacement due to LIDAR rotation
%         u=[0 sin(phi*k/N) cos(phi*k/N)]*[cos(theta) 0 sin(theta);0 1 0;-sin(theta) 0 cos(theta)]; % unit vector w.r.t. LIDAR co-ordinate
%         R(:,k,j)=Translation+u*Rotation;                                                                 % unit vector w.r.t. Camera co-ordinate
%     end
%     j=j+1;
% end
% for j=1:11
%     for k=1:64
%         quiver3(Translation(1,1),Translation(1,2),Translation(1,3),R(1,k,j),R(2,k,j),R(3,k,j));
%         %vectarrow(Translation,Translation+R(:,k,j)')
%         hold on;
%     end
%     hold on;
% end
%
% % for i=1:64
% %     for s=0.:0.02:20;
% %     plot(s,s.*R(:,i))
% %     hold on;
% %     end
% % end