function [ n_elements ] = ComparePoints( src_file,tgt_file )
%Compare Points by Harry
%Src_file contains laser points file, and tgt_file contains computed points file.
src=textread(src_file);
tgt=textread(tgt_file);
src=src';
tgt=tgt';
%remove (0,0,0)
src(:,all(src==0,1))=[];
tgt(:,all(tgt==0,1))=[];
ptNum=size(src,2);
o=ones(1,ptNum);
nsrc=[src;o];
ntgt=[tgt;o];
%remove outliers, any component whose difference is larger than 20
threshold=20;
nsrc_clean=nsrc;
ntgt_clean=ntgt;
dif=nsrc-ntgt;
nsrc_clean(:,find(abs(dif(1,:))>threshold | abs(dif(2,:))>threshold | abs(dif(3,:))>threshold))=[];
ntgt_clean(:,find(abs(dif(1,:))>threshold | abs(dif(2,:))>threshold | abs(dif(3,:))>threshold))=[];
%fit a transform
t_clean=ntgt_clean/nsrc_clean;
nsrc_clean_t=t_clean*nsrc_clean;
%linear fit
nsrc_clean_t_abs_square=sum(nsrc_clean_t.*nsrc_clean_t)-1;
nsrc_clean_t_abs=sqrt(nsrc_clean_t_abs_square);
ntgt_clean_abs_square=sum(ntgt_clean.*ntgt_clean)-1;
ntgt_clean_abs=sqrt(ntgt_clean_abs_square);
p=polyfit(nsrc_clean_t_abs,ntgt_clean_abs,1);
%compute difference
nsrc_clean_t_abs_fit=p(1)*nsrc_clean_t_abs+p(2);
diff_clean_t_abs_fit=nsrc_clean_t_abs_fit-ntgt_clean_abs;
diff_clean_t_abs_fit_ratio=abs(diff_clean_t_abs_fit./nsrc_clean_t_abs_fit);
intervals=0:0.05:0.5;
n_elements=histc(diff_clean_t_abs_fit_ratio,intervals);
dlmwrite('nsrc_clean_t.txt',nsrc_clean_t);
end

