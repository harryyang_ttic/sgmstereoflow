#include <iostream>
#include <fstream>
#include <string>
#include <cmdline.h>
#include <revlib.h>
#include "MatchedPoint.h"
#include "SiftMatcher.h"
#include "FundamentalMatrix.h"

struct ParameterSiftFund {
    bool verbose;
    bool inoutSiftFile;
    std::string firstImageFilename;
    std::string secondImageFilename;
    std::string outputFundamentalMatrixFilename;
};

// Prototype declaration
cmdline::parser makeCommandParser();
ParameterSiftFund parseCommandline(int argc, char* argv[]);

cmdline::parser makeCommandParser() {
    cmdline::parser commandParser;
    commandParser.add<std::string>("output", 'o', "output file of fundamental matrix", false, "");
    commandParser.add("verbose", 'v', "verbose");
    commandParser.add("help", 'h', "display this message");
    commandParser.footer("first_image second_image");
    commandParser.set_program_name("siftfund");
    
    return commandParser;
}

ParameterSiftFund parseCommandline(int argc, char* argv[]) {
    // Make command parser
    cmdline::parser commandParser = makeCommandParser();
    // Parse command line
    bool isCorrectCommand = commandParser.parse(argc, argv);
    if (!isCorrectCommand) {
        std::cerr << commandParser.error() << std::endl;
    }
    if (!isCorrectCommand || commandParser.exist("help") || commandParser.rest().size() < 2) {
        std::cerr << commandParser.usage() << std::endl;
        exit(1);
    }
    
    // Set program parameters
    ParameterSiftFund parameters;
    // Verbose flag
    parameters.verbose = commandParser.exist("verbose");
    // Input images
    parameters.firstImageFilename = commandParser.rest()[0];
    parameters.secondImageFilename = commandParser.rest()[1];
    // Output file
    std::string outputFundamentalMatrixFilename = commandParser.get<std::string>("output");
    if (outputFundamentalMatrixFilename == "") {
        outputFundamentalMatrixFilename = parameters.firstImageFilename;
        size_t dotPosition = outputFundamentalMatrixFilename.rfind('.');
        if (dotPosition != std::string::npos) outputFundamentalMatrixFilename.erase(dotPosition);
        outputFundamentalMatrixFilename += "_fund.dat";
    }
    parameters.outputFundamentalMatrixFilename = outputFundamentalMatrixFilename;
    
    return parameters;
}

int main(int argc, char* argv[]) {
    // Parse command line
    ParameterSiftFund parameters = parseCommandline(argc, argv);
    
    // Output parameters
    if (parameters.verbose) {
        std::cerr << std::endl;
        std::cerr << "First image:               " << parameters.firstImageFilename << std::endl;
        std::cerr << "Second image:              " << parameters.secondImageFilename << std::endl;
        std::cerr << "Output fundamental matrix: " << parameters.outputFundamentalMatrixFilename << std::endl;
        std::cerr << std::endl;
    }
    
    try {
        // Open images
        rev::Image<unsigned char> firstImage = rev::readImageFileGrayscale(parameters.firstImageFilename);
        rev::Image<unsigned char> secondImage = rev::readImageFileGrayscale(parameters.secondImageFilename);
        
        // Match keypoints
        std::vector<MatchedPoint> matchedPoints;
        rev::Sift sift;
        std::vector<rev::Keypoint> firstKeypoints;
        std::vector< std::vector<unsigned char> > firstDescriptors;
        sift.detect(firstImage, firstKeypoints, firstDescriptors);
        std::vector<rev::Keypoint> secondKeypoints;
        std::vector< std::vector<unsigned char> > secondDescriptors;
        sift.detect(secondImage, secondKeypoints, secondDescriptors);
            
        SiftMatcher siftMatcher;
        siftMatcher.match(firstKeypoints, firstDescriptors, secondKeypoints, secondDescriptors, matchedPoints);
        
        // Estimate fundamental matrix;
        FundamentalMatrix fundamentalMatrix;
        fundamentalMatrix.estimateSelectionLMedS(matchedPoints);
        fundamentalMatrix.estimateEpipolarSearchDirection(firstImage.width(), firstImage.height(), matchedPoints);
        
        // Output
        fundamentalMatrix.writeFundamentalMatrixFile(parameters.outputFundamentalMatrixFilename);
        
    } catch (const rev::Exception& revException) {
        std::cerr << "Error [" << revException.functionName() << "]: " << revException.message() << std::endl;
        exit(1);
    }
}
