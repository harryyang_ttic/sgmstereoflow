#include "SobelFilter.h"
#include <emmintrin.h>

void SobelFilter::filterVerticalHorizontal(const rev::Image<unsigned char>& inputImage,
										   rev::Image<short>& horizontalSobelImage,
										   rev::Image<short>& verticalSobelImage) const
{
	if (inputImage.channelTotal() != 1) {
		throw rev::Exception("SobelFilter::filterVerticalHorizontal", "input image must be grayscale");
	}

	filterSSE(inputImage, horizontalSobelImage);
	verticalFilter(inputImage, verticalSobelImage);
}


void SobelFilter::filterSSE(const rev::Image<unsigned char>& image, rev::Image<short>& horizontalFilteredImage) const {
	int width = image.width();
	int height = image.height();
	int widthStep = image.widthStep();

	horizontalFilteredImage.resize(width, height, 1);
	horizontalFilteredImage.setTo(0);

	// Sobel filter
	//short* tempBuffer = reinterpret_cast<short*>(_mm_malloc(widthStep*height*sizeof(short), 16));
	//convolveVerticalKernel(image.dataPointer(), widthStep, height, tempBuffer);
	//convolveHorizontalKernel(tempBuffer, widthStep, height, horizontalFilteredImage.dataPointer());
	//_mm_free(tempBuffer);
	// Sobel filter
	for (int y = 1; y < height - 1; ++y) {
		for (int x = 1; x < width - 1; ++x) {
			horizontalFilteredImage(x, y) = (image(x + 1, y) - image(x - 1, y))*2
				+ image(x + 1, y - 1) - image(x - 1, y - 1)
				+ image(x + 1, y + 1) - image(x + 1, y + 1);
		}
	}
}

void SobelFilter::verticalFilter(const rev::Image<unsigned char>& image, rev::Image<short>& verticalFilteredImage) const {
	int width = image.width();
	int height = image.height();

	verticalFilteredImage.resize(width, height, 1);
	verticalFilteredImage.setTo(0);

	// Sobel filter
	for (int y = 1; y < height - 1; ++y) {
		for (int x = 1; x < width - 1; ++x) {
			verticalFilteredImage(x, y) = (image(x, y + 1) - image(x, y - 1))*2
				+ image(x - 1, y + 1) - image(x - 1, y - 1)
				+ image(x + 1, y + 1) - image(x + 1, y - 1);
		}
	}
}

void SobelFilter::convolveVerticalKernel(const unsigned char* imageData,
										 const int width, const int height,
										 short* output) const
{
	unsigned char* input = const_cast<unsigned char*>(imageData);
	__m128i zeroRegister = _mm_setzero_si128();
	for (int i = 0; i < width*(height - 2); i += 16) {
		__m128i topRow = _mm_load_si128(reinterpret_cast<__m128i*>(input + i));
		__m128i centerRow = _mm_load_si128(reinterpret_cast<__m128i*>(input + width + i));
		__m128i bottomRow = _mm_load_si128(reinterpret_cast<__m128i*>(input + width*2 + i));

		__m128i resultLowBit = _mm_setzero_si128();
		__m128i resultHighBit = _mm_setzero_si128();

		// Top row
		__m128i lowBitRegister = _mm_unpacklo_epi8(topRow, zeroRegister);
		__m128i highBitRegister = _mm_unpackhi_epi8(topRow, zeroRegister);
		resultLowBit = _mm_add_epi16(resultLowBit, lowBitRegister);
		resultHighBit = _mm_add_epi16(resultHighBit, highBitRegister);
		// Center row*2
		lowBitRegister = _mm_unpacklo_epi8(centerRow, zeroRegister);
		highBitRegister = _mm_unpackhi_epi8(centerRow, zeroRegister);
		resultLowBit = _mm_add_epi16(resultLowBit, lowBitRegister);
		resultLowBit = _mm_add_epi16(resultLowBit, lowBitRegister);
		resultHighBit = _mm_add_epi16(resultHighBit, highBitRegister);
		resultHighBit = _mm_add_epi16(resultHighBit, highBitRegister);
		// Bottom row
		lowBitRegister = _mm_unpacklo_epi8(bottomRow, zeroRegister);
		highBitRegister = _mm_unpackhi_epi8(bottomRow, zeroRegister);
		resultLowBit = _mm_add_epi16(resultLowBit, lowBitRegister);
		resultHighBit = _mm_add_epi16(resultHighBit, highBitRegister);

		_mm_store_si128(reinterpret_cast<__m128i*>(output + width + i), resultLowBit);
		_mm_store_si128(reinterpret_cast<__m128i*>(output + width + i + 8), resultHighBit);
	}
}

void SobelFilter::convolveHorizontalKernel(short* input, const int width, const int height, short* output) const {
	for (int i = 0; i < width*height - 2; i += 8) {
		__m128i leftColumn = _mm_load_si128(reinterpret_cast<__m128i*>(input + i));
		__m128i rightColumn = _mm_loadu_si128(reinterpret_cast<__m128i*>(input + i + 2));
		__m128i resultRegister = _mm_sub_epi16(rightColumn, leftColumn);

		_mm_storeu_si128(reinterpret_cast<__m128i*>(output + i + 1), resultRegister);
	}
}
