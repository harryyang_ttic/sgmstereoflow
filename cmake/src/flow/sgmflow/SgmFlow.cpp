#include "SgmFlow.h"
#include <stack>
#include <float.h>
#include <emmintrin.h>
#include "EpipolarGCCostCalculator.h"

// Default parameters
const double SGMFLOW_DEFAULT_MAX_VZ_RATIO = 0.3;
const int SGMFLOW_DEFAULT_DISCRETIZATION_TOTAL = 256;
const int SGMFLOW_DEFAULT_SOBEL_CAP_VALUE = 15;
const int SGMFLOW_DEFAULT_CENSUS_WINDOW_RADIUS = 2;
const double SGMFLOW_DEFAULT_CENSUS_WEIGHT_FACTOR = 1.0/2.0;
const int SGMFLOW_DEFAULT_AGGREGATION_WINDOW_RADIUS = 2;
const int SGMFLOW_DEFAULT_SMOOTHNESS_PENALTY_SMALL = 4*(SGMFLOW_DEFAULT_AGGREGATION_WINDOW_RADIUS*2 + 1)*(SGMFLOW_DEFAULT_AGGREGATION_WINDOW_RADIUS*2 + 1);
const int SGMFLOW_DEFAULT_SMOOTHNESS_PENALTY_LARGE = 64*(SGMFLOW_DEFAULT_AGGREGATION_WINDOW_RADIUS*2 + 1)*(SGMFLOW_DEFAULT_AGGREGATION_WINDOW_RADIUS*2 + 1);
const bool SGMFLOW_DEFAULT_PATH_EIGHT_DIRECTIONS = false;
const int SGMFLOW_DEFAULT_CONSISTENCY_THRESHOLD = 2;

const double outputVZIndexFactor = 256.0;

SgmFlow::SgmFlow() : maxVZRatio_(SGMFLOW_DEFAULT_MAX_VZ_RATIO),
	discretizationTotal_(SGMFLOW_DEFAULT_DISCRETIZATION_TOTAL),
	sobelCapValue_(SGMFLOW_DEFAULT_SOBEL_CAP_VALUE),
	censusWindowRadius_(SGMFLOW_DEFAULT_CENSUS_WINDOW_RADIUS),
	censusWeightFactor_(SGMFLOW_DEFAULT_CENSUS_WEIGHT_FACTOR),
	aggregationWindowRadius_(SGMFLOW_DEFAULT_AGGREGATION_WINDOW_RADIUS),
	smoothnessPenaltySmall_(SGMFLOW_DEFAULT_SMOOTHNESS_PENALTY_SMALL),
	smoothnessPenaltyLarge_(SGMFLOW_DEFAULT_SMOOTHNESS_PENALTY_LARGE),
	pathEightDirections_(SGMFLOW_DEFAULT_PATH_EIGHT_DIRECTIONS),
	consistencyThreshold_(SGMFLOW_DEFAULT_CONSISTENCY_THRESHOLD) {}

void SgmFlow::setVZRatioParameters(const double maxVZRatio, const int discretizationTotal) {
	if (maxVZRatio <= 0.0 || maxVZRatio > 1.0) {
		throw rev::Exception("SgmFlow::setVZRatioParameters", "max value of VZ-ratio is out of range");
	}
	maxVZRatio_ = maxVZRatio;

	if (discretizationTotal <= 0 || discretizationTotal%16 != 0) {
		throw rev::Exception("SgmFlow::setVZRatioParameters", "the number of discretization must be a multiple of 16");
	}
	discretizationTotal_ = discretizationTotal;
}

void SgmFlow::setDataCostParameters(const int sobelCapValue,
									const int censusWindowRadius, const int censusWeightFactor,
									const int aggregationWindowRadius)
{
	// Cap value of sobel filter
	sobelCapValue_ = std::max(sobelCapValue, 15);
	sobelCapValue_ = std::min(sobelCapValue_, 127) | 1;

	// Window size of Census transform
	if (censusWindowRadius != 1 && censusWindowRadius != 2) {
		throw rev::Exception("SgmFlow::setDataCostParameters", "window size of census transform must be 1 or 2");
	}
	censusWindowRadius_ = censusWindowRadius;

	// Cost weight of Census
	if (censusWeightFactor < 0) {
		throw rev::Exception("SgmFlow::setDataCostParameters", "weight of census is less than zero");
	}
	censusWeightFactor_ = censusWeightFactor;

	// Window radius for aggregating pixelwise costs
	if (aggregationWindowRadius < 0) {
		throw rev::Exception("SgmFlow::setDataCostParameters", "window size of cost aggregation is less than zero");
	}
	aggregationWindowRadius_ = aggregationWindowRadius;
}

void SgmFlow::setSmoothnessCostParameters(const int smoothnessPenaltySmall, const int smoothnessPenaltyLarge)
{
	if (smoothnessPenaltySmall < 0 || smoothnessPenaltyLarge < 0) {
		throw rev::Exception("SgmFlow::setSmoothnessCostParameters", "smoothness penalty value is less than zero");
	}
	if (smoothnessPenaltySmall >= smoothnessPenaltyLarge) {
		throw rev::Exception("SgmFlow::setSmoothnessCostParameters",
			"small value of smoothness penalty must be smaller than large penalty value");
	}

	smoothnessPenaltySmall_ = smoothnessPenaltySmall;
	smoothnessPenaltyLarge_ = smoothnessPenaltyLarge;
}

void SgmFlow::setSgmParameters(const bool pathEightDirections, const int consistencyThreshold) {
	pathEightDirections_ = pathEightDirections;

	if (consistencyThreshold < 0) {
		throw rev::Exception("SgmFlow::setSgmParameters", "Threshold for LR consistency must be positive");
	}
	consistencyThreshold_ = consistencyThreshold;
}

void SgmFlow::compute(const rev::Image<unsigned char>& firstImage,
						   const rev::Image<unsigned char>& secondImage,
						   const CameraMotion& cameraMotion,
						   rev::Image<unsigned short>& firstVZIndexImage,
						   rev::Image<unsigned short>& secondVZIndexImage,
						   rev::Image<unsigned short>& firstFlowImage)
{
	checkInputImage(firstImage, secondImage);
	// Image size
	int width = firstImage.width();
	int height = firstImage.height();

	// Epipolar flow geometry
	CalibratedEpipolarFlowGeometry epipolarFlowGeometry(width, height, cameraMotion);
	// Compute cost images
	EpipolarGCCostCalculator costCalculator(maxVZRatio_, discretizationTotal_,
		sobelCapValue_,
		censusWindowRadius_, censusWeightFactor_,
		aggregationWindowRadius_);
	rev::Image<unsigned short> costImage;
	costCalculator.computeCostImage(firstImage, secondImage, epipolarFlowGeometry, costImage);

	// SGM
	allocateBuffer(width, height);
	performSGM(costImage, firstVZIndexImage);

	// Consistency check
	convertVZIndexImageFromFirstToSecond(epipolarFlowGeometry, firstVZIndexImage, secondVZIndexImage);
	enforceLeftRightConsistency(epipolarFlowGeometry, firstVZIndexImage, secondVZIndexImage);
	speckleFilter(width*height/10, discretizationTotal_*outputVZIndexFactor, firstVZIndexImage);

	// Make second discretized image
	convertVZIndexImageFromFirstToSecond(epipolarFlowGeometry, firstVZIndexImage, secondVZIndexImage);
	// Make flow images
	makeFlowImage(epipolarFlowGeometry, firstVZIndexImage, firstFlowImage);
}


void SgmFlow::checkInputImage(const rev::Image<unsigned char>& firstImage, const rev::Image<unsigned char>& secondImage) const {
	if (firstImage.width() != secondImage.width() || firstImage.height() != secondImage.height()) {
		throw rev::Exception("SgmFlow::checkInputImages", "sizes of input images are not the same");
	}
}


void SgmFlow::allocateBuffer(const int width, const int height) {
	setBufferSize(width, height);

	buffer_.resize(totalBufferSize_);
}

void SgmFlow::performSGM(const rev::Image<unsigned short>& costImage, rev::Image<unsigned short> &vzIndexImage) {
	const short costMax = SHRT_MAX;

	int width = costImage.width();
	int height = costImage.height();

	unsigned short* costImageData = costImage.dataPointer();
	int widthStepCostImage = costImage.widthStep();

	vzIndexImage.resize(width, height, 1);
	unsigned short* vzIndexImageData = vzIndexImage.dataPointer();
	int widthStepVZIndexImage = vzIndexImage.widthStep();

	short* costSums = buffer_.pointer();
	memset(costSums, 0, costSumBufferSize_*sizeof(short));

	short** pathCosts = new short* [pathRowBufferTotal_];
	short** pathMinCosts = new short* [pathRowBufferTotal_];

	const int processPassTotal = 2;
	for (int processPassCount = 0; processPassCount < processPassTotal; ++processPassCount) {
		int startX, endX, stepX;
		int startY, endY, stepY;
		if (processPassCount == 0) {
			startX = 0; endX = width; stepX = 1;
			startY = 0; endY = height; stepY = 1;
		} else {
			startX = width - 1; endX = -1; stepX = -1;
			startY = height - 1; endY = -1; stepY = -1;
		}

		for (int i = 0; i < pathRowBufferTotal_; ++i) {
			pathCosts[i] = costSums + costSumBufferSize_ + pathCostBufferSize_*i + pathVZIndexSize_ + 8;
			memset(pathCosts[i] - pathVZIndexSize_ - 8, 0, pathCostBufferSize_*sizeof(short));
			pathMinCosts[i] = costSums + costSumBufferSize_ + pathCostBufferSize_*pathRowBufferTotal_
				+ pathMinCostBufferSize_*i + pathTotal_*2;
			memset(pathMinCosts[i] - pathTotal_, 0, pathMinCostBufferSize_*sizeof(short));
		}

		for (int y = startY; y != endY; y += stepY) {
			unsigned short* vzIndexRow = vzIndexImageData + widthStepVZIndexImage*y;
			unsigned short* pixelCostRow = costImageData + widthStepCostImage*y;
			short* costSumRow = costSums + costSumBufferRowSize_*y;

			memset(pathCosts[0] - pathVZIndexSize_ - 8, 0, pathVZIndexSize_*sizeof(short));
			memset(pathCosts[0] + width*pathVZIndexSize_ - 8, 0, pathVZIndexSize_*sizeof(short));
			memset(pathMinCosts[0] - pathTotal_, 0, pathTotal_*sizeof(short));
			memset(pathMinCosts[0] + width*pathTotal_, 0, pathTotal_*sizeof(short));

			for (int x = startX; x != endX; x += stepX) {
				int pathMinX = x*pathTotal_;
				int pathX = pathMinX*vzIndexSize_;

				int previousPathMin0, previousPathMin1, previousPathMin2, previousPathMin3;
				previousPathMin0 = pathMinCosts[0][pathMinX - stepX*pathTotal_] + smoothnessPenaltyLarge_;
				previousPathMin2 = pathMinCosts[1][pathMinX + 2] + smoothnessPenaltyLarge_;

				short *previousPathCosts0, *previousPathCosts1, *previousPathCosts2, *previousPathCosts3;
				previousPathCosts0 = pathCosts[0] + pathX - stepX*pathVZIndexSize_;
				previousPathCosts2 = pathCosts[1] + pathX + vzIndexSize_*2;

				previousPathCosts0[-1] = previousPathCosts0[discretizationTotal_] = costMax;
				previousPathCosts2[-1] = previousPathCosts2[discretizationTotal_] = costMax;

				if (pathEightDirections_) {
					previousPathMin1 = pathMinCosts[1][pathMinX - pathTotal_ + 1] + smoothnessPenaltyLarge_;
					previousPathMin3 = pathMinCosts[1][pathMinX + pathTotal_ + 3] + smoothnessPenaltyLarge_;

					previousPathCosts1 = pathCosts[1] + pathX - pathVZIndexSize_ + vzIndexSize_;
					previousPathCosts3 = pathCosts[1] + pathX + pathVZIndexSize_ + vzIndexSize_*3;

					previousPathCosts1[-1] = previousPathCosts1[discretizationTotal_] = costMax;
					previousPathCosts3[-1] = previousPathCosts3[discretizationTotal_] = costMax;
				}

				short* pathCostCurrent = pathCosts[0] + pathX;
				const unsigned short* pixelCostCurrent = pixelCostRow + discretizationTotal_*x;
				short* costSumCurrent = costSumRow + discretizationTotal_*x;

				__m128i regPenaltySmall = _mm_set1_epi16(static_cast<short>(smoothnessPenaltySmall_));

				__m128i regPathMin0, regPathMin1, regPathMin2, regPathMin3;
				regPathMin0 = _mm_set1_epi16(static_cast<short>(previousPathMin0));
				regPathMin2 = _mm_set1_epi16(static_cast<short>(previousPathMin2));
				if (pathEightDirections_) {
					regPathMin1 = _mm_set1_epi16(static_cast<short>(previousPathMin1));
					regPathMin3 = _mm_set1_epi16(static_cast<short>(previousPathMin3));
				}
				__m128i regNewPathMin = _mm_set1_epi16(costMax);

				for (int d = 0; d < discretizationTotal_; d += 8) {
					__m128i regPixelCost = _mm_load_si128(reinterpret_cast<const __m128i*>(pixelCostCurrent + d));

					__m128i regPathCost0, regPathCost1, regPathCost2, regPathCost3;
					regPathCost0 = _mm_load_si128(reinterpret_cast<const __m128i*>(previousPathCosts0 + d));
					regPathCost2 = _mm_load_si128(reinterpret_cast<const __m128i*>(previousPathCosts2 + d));

					regPathCost0 = _mm_min_epi16(regPathCost0,
						_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts0 + d - 1)),
						regPenaltySmall));
					regPathCost0 = _mm_min_epi16(regPathCost0,
						_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts0 + d + 1)),
						regPenaltySmall));
					regPathCost2 = _mm_min_epi16(regPathCost2,
						_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts2 + d - 1)),
						regPenaltySmall));
					regPathCost2 = _mm_min_epi16(regPathCost2,
						_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts2 + d + 1)),
						regPenaltySmall));

					regPathCost0 = _mm_min_epi16(regPathCost0, regPathMin0);
					regPathCost0 = _mm_adds_epi16(_mm_subs_epi16(regPathCost0, regPathMin0), regPixelCost);
					regPathCost2 = _mm_min_epi16(regPathCost2, regPathMin2);
					regPathCost2 = _mm_adds_epi16(_mm_subs_epi16(regPathCost2, regPathMin2), regPixelCost);

					_mm_store_si128(reinterpret_cast<__m128i*>(pathCostCurrent + d), regPathCost0);
					_mm_store_si128(reinterpret_cast<__m128i*>(pathCostCurrent + d + vzIndexSize_*2), regPathCost2);

					if (pathEightDirections_) {
						regPathCost1 = _mm_load_si128(reinterpret_cast<const __m128i*>(previousPathCosts1 + d));
						regPathCost3 = _mm_load_si128(reinterpret_cast<const __m128i*>(previousPathCosts3 + d));

						regPathCost1 = _mm_min_epi16(regPathCost1,
							_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts1 + d - 1)),

							regPenaltySmall));
						regPathCost1 = _mm_min_epi16(regPathCost1,
							_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts1 + d + 1)),
							regPenaltySmall));
						regPathCost3 = _mm_min_epi16(regPathCost3,
							_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts3 + d - 1)),
							regPenaltySmall));
						regPathCost3 = _mm_min_epi16(regPathCost3,
							_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts3 + d + 1)),
							regPenaltySmall));

						regPathCost1 = _mm_min_epi16(regPathCost1, regPathMin1);
						regPathCost1 = _mm_adds_epi16(_mm_subs_epi16(regPathCost1, regPathMin1), regPixelCost);
						regPathCost3 = _mm_min_epi16(regPathCost3, regPathMin3);
						regPathCost3 = _mm_adds_epi16(_mm_subs_epi16(regPathCost3, regPathMin3), regPixelCost);

						_mm_store_si128(reinterpret_cast<__m128i*>(pathCostCurrent + d + vzIndexSize_), regPathCost1);
						_mm_store_si128(reinterpret_cast<__m128i*>(pathCostCurrent + d + vzIndexSize_*3), regPathCost3);
					}

					__m128i regMin02 = _mm_min_epi16(_mm_unpacklo_epi16(regPathCost0, regPathCost2),
						_mm_unpackhi_epi16(regPathCost0, regPathCost2));
					if (pathEightDirections_) {
						__m128i regMin13 = _mm_min_epi16(_mm_unpacklo_epi16(regPathCost1, regPathCost3),
							_mm_unpackhi_epi16(regPathCost1, regPathCost3));
						regMin02 = _mm_min_epi16(_mm_unpacklo_epi16(regMin02, regMin13),
							_mm_unpackhi_epi16(regMin02, regMin13));
					} else {
						regMin02 = _mm_min_epi16(_mm_unpacklo_epi16(regMin02, regMin02),
							_mm_unpackhi_epi16(regMin02, regMin02));
					}
					regNewPathMin = _mm_min_epi16(regNewPathMin, regMin02);

					__m128i regCostSum = _mm_load_si128(reinterpret_cast<const __m128i*>(costSumCurrent + d));

					if (pathEightDirections_) {
						regPathCost0 = _mm_adds_epi16(regPathCost0, regPathCost1);
						regPathCost2 = _mm_adds_epi16(regPathCost2, regPathCost3);
					}
					regCostSum = _mm_adds_epi16(regCostSum, regPathCost0);
					regCostSum = _mm_adds_epi16(regCostSum, regPathCost2);

					_mm_store_si128(reinterpret_cast<__m128i*>(costSumCurrent + d), regCostSum);
				}

				regNewPathMin = _mm_min_epi16(regNewPathMin, _mm_srli_si128(regNewPathMin, 8));
				_mm_storel_epi64(reinterpret_cast<__m128i*>(&pathMinCosts[0][pathMinX]), regNewPathMin);
			}

			if (processPassCount == processPassTotal - 1) {
				for (int x = 0; x < width; ++x) {
					short* costSumCurrent = costSumRow + discretizationTotal_*x;
					int bestSumCost = costSumCurrent[0];
					int bestVZIndex = 0;
					for (int d = 1; d < discretizationTotal_; ++d) {
						if (costSumCurrent[d] < bestSumCost) {
							bestSumCost = costSumCurrent[d];
							bestVZIndex = d;
						}
					}

					if (bestVZIndex > 0 && bestVZIndex < discretizationTotal_ - 1) {
						int centerCostValue = costSumCurrent[bestVZIndex];
						int leftCostValue = costSumCurrent[bestVZIndex - 1];
						int rightCostValue = costSumCurrent[bestVZIndex + 1];
						if (rightCostValue < leftCostValue) {
							bestVZIndex = static_cast<int>(bestVZIndex*outputVZIndexFactor
								+ static_cast<double>(rightCostValue - leftCostValue)/(centerCostValue - leftCostValue)/2.0*outputVZIndexFactor + 0.5);
						} else {
							bestVZIndex = static_cast<int>(bestVZIndex*outputVZIndexFactor
								+ static_cast<double>(rightCostValue - leftCostValue)/(centerCostValue - rightCostValue)/2.0*outputVZIndexFactor + 0.5);
						}
					} else {
						bestVZIndex *= outputVZIndexFactor;
					}

					vzIndexRow[x] = static_cast<unsigned short>(bestVZIndex);
				}
			}

			std::swap(pathCosts[0], pathCosts[1]);
			std::swap(pathMinCosts[0], pathMinCosts[1]);
		}
	}
	delete [] pathCosts;
	delete [] pathMinCosts;

	speckleFilter(100, 2*outputVZIndexFactor, vzIndexImage);
}

void SgmFlow::convertVZIndexImageFromFirstToSecond(const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
														const rev::Image<unsigned short>& firstVZIndexImage,
												   rev::Image<unsigned short>& secondVZIndexImage) const
{
	double coefficientForVZRatio = maxVZRatio_/discretizationTotal_/outputVZIndexFactor;

	int width = firstVZIndexImage.width();
	int height = firstVZIndexImage.height();

	secondVZIndexImage.resize(width, height, 1);
	secondVZIndexImage.setTo(0);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			if (firstVZIndexImage(x, y) == 0) continue;

			double vzRatio = static_cast<double>(firstVZIndexImage(x, y))*coefficientForVZRatio;
			double disparityValue = epipolarFlowGeometry.distanceFromEpipole(x, y)*vzRatio/(1 - vzRatio);
			int secondX = static_cast<int>(epipolarFlowGeometry.noDisparityPointX(x, y)
				+ epipolarFlowGeometry.epipolarDirectionX(x, y)*disparityValue + 0.5);
			int secondY = static_cast<int>(epipolarFlowGeometry.noDisparityPointY(x, y)
				+ epipolarFlowGeometry.epipolarDirectionY(x, y)*disparityValue + 0.5);

			int sx0 = static_cast<int>(floor(secondX));
			int sx1 = sx0 + 1;
			int sy0 = static_cast<int>(floor(secondY));
			int sy1 = sy0 + 1;

			if (sx0 >= 0 && sx0 < width && sy0 >= 0 && sy0 < height
				&& (secondVZIndexImage(sx0, sy0) == 0 || secondVZIndexImage(sx0, sy0) < firstVZIndexImage(x, y)))
			{
				secondVZIndexImage(sx0, sy0) = firstVZIndexImage(x, y);
			}
			if (sx1 >= 0 && sx1 < width && sy0 >= 0 && sy0 < height
				&& (secondVZIndexImage(sx1, sy0) == 0 || secondVZIndexImage(sx1, sy0) < firstVZIndexImage(x, y)))
			{
				secondVZIndexImage(sx1, sy0) = firstVZIndexImage(x, y);
			}
			if (sx0 >= 0 && sx0 < width && sy1 >= 0 && sy1 < height
				&& (secondVZIndexImage(sx0, sy1) == 0 || secondVZIndexImage(sx0, sy1) < firstVZIndexImage(x, y)))
			{
				secondVZIndexImage(sx0, sy1) = firstVZIndexImage(x, y);
			}
			if (sx1 >= 0 && sx1 < width && sy1 >= 0 && sy1 < height
				&& (secondVZIndexImage(sx1, sy1) == 0 || secondVZIndexImage(sx1, sy1) < firstVZIndexImage(x, y)))
			{
				secondVZIndexImage(sx1, sy1) = firstVZIndexImage(x, y);
			}
		}
	}
}

void SgmFlow::enforceLeftRightConsistency(const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
										  rev::Image<unsigned short>& firstVZIndexImage,
										  rev::Image<unsigned short>& secondVZIndexImage) const
{
	const int vzIndexConsistencyThreshold = consistencyThreshold_*outputVZIndexFactor;

	int width = firstVZIndexImage.width();
	int height = firstVZIndexImage.height();

	double coefficientForVZRatio = maxVZRatio_/discretizationTotal_/outputVZIndexFactor;

	// Check first disparity image
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			if (firstVZIndexImage(x, y) == 0) continue;

			double firstVZRatio =static_cast<double>(firstVZIndexImage(x, y))*coefficientForVZRatio;
			double firstDisparityValue = epipolarFlowGeometry.distanceFromEpipole(x, y)*firstVZRatio/(1 - firstVZRatio);
			double secondX = epipolarFlowGeometry.noDisparityPointX(x, y)
				+ epipolarFlowGeometry.epipolarDirectionX(x, y)*firstDisparityValue;
			double secondY = epipolarFlowGeometry.noDisparityPointY(x, y)
				+ epipolarFlowGeometry.epipolarDirectionY(x, y)*firstDisparityValue;
			int secondXInt = static_cast<int>(secondX + 0.5);
			int secondYInt = static_cast<int>(secondY + 0.5);

			if (secondX < 0 || secondXInt >= width || secondY < 0 || secondYInt >= height) {
				firstVZIndexImage(x, y) = 0;
				continue;
			}
			if (secondVZIndexImage(secondXInt, secondYInt) == 0) {
				firstVZIndexImage(x, y) = 0;
				continue;
			}

			if (abs(firstVZIndexImage(x, y) - secondVZIndexImage(secondXInt, secondYInt)) > vzIndexConsistencyThreshold) {
				firstVZIndexImage(x, y) = 0;
			}
		}
	}
}

void SgmFlow::makeFlowImage(const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
							const rev::Image<unsigned short>& firstVZIndexImage,
							rev::Image<unsigned short>& firstFlowImage) const
{
	double coefficientForVZRatio = maxVZRatio_/discretizationTotal_/outputVZIndexFactor;

	rev::Image<unsigned short> interpolatedFirstVZIndexImage;
	interpolateVZIndexImage(firstVZIndexImage, interpolatedFirstVZIndexImage);

	int width = interpolatedFirstVZIndexImage.width();
	int height = interpolatedFirstVZIndexImage.height();

	firstFlowImage.resize(width, height, 3);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			// First flow image
			if (interpolatedFirstVZIndexImage(x, y) == 0) {
				firstFlowImage(x, y, 0) = 0;
				firstFlowImage(x, y, 1) = 0;
				firstFlowImage(x, y, 2) = 0;
			} else {
				double vzRatio = static_cast<double>(interpolatedFirstVZIndexImage(x, y))*coefficientForVZRatio;
				double disparityValue = epipolarFlowGeometry.distanceFromEpipole(x, y)*vzRatio/(1 - vzRatio);
				double secondX = epipolarFlowGeometry.noDisparityPointX(x, y)
					+ epipolarFlowGeometry.epipolarDirectionX(x, y)*disparityValue;
				double secondY = epipolarFlowGeometry.noDisparityPointY(x, y)
					+ epipolarFlowGeometry.epipolarDirectionY(x, y)*disparityValue;
				double firstFlowX = secondX - x;
				double firstFlowY = secondY - y;

				firstFlowImage(x, y, 0) = static_cast<unsigned short>(firstFlowX*64.0 + 32768.5);
				firstFlowImage(x, y, 1) = static_cast<unsigned short>(firstFlowY*64.0 + 32768.5);
				firstFlowImage(x, y, 2) = 1;
			}
		}
	}
}

void SgmFlow::setBufferSize(const int width, const int height) {
	pathRowBufferTotal_ = 2;
	vzIndexSize_ = discretizationTotal_ + 16;
	pathTotal_ = 8;
	pathVZIndexSize_ = pathTotal_*vzIndexSize_;

	costSumBufferRowSize_ = width*discretizationTotal_;
	costSumBufferSize_ = costSumBufferRowSize_*height;
	pathMinCostBufferSize_ = (width + 2)*pathTotal_;
	pathCostBufferSize_ = pathMinCostBufferSize_*vzIndexSize_;
	totalBufferSize_ = (pathMinCostBufferSize_ + pathCostBufferSize_)*pathRowBufferTotal_ + costSumBufferSize_ + 16;
}

void SgmFlow::speckleFilter(const int maxSpeckleSize, const int maxDifference,
							rev::Image<unsigned short>& image) const
{
	int width = image.width();
	int height = image.height();

	std::vector<int> labels(width*height, 0);
	std::vector<bool> regionTypes(1);
	regionTypes[0] = false;

	int currentLabelIndex = 0;

	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			int pixelIndex = width*y + x;
			if (image(x, y) != 0) {
				if (labels[pixelIndex] > 0) {
					if (regionTypes[labels[pixelIndex]]) {
						image(x, y) = 0;
					}
				}else {
					std::stack<int> wavefrontIndices;
					wavefrontIndices.push(pixelIndex);
					++currentLabelIndex;
					regionTypes.push_back(false);
					int regionPixelTotal = 0;
					labels[pixelIndex] = currentLabelIndex;

					while (!wavefrontIndices.empty()) {
						int currentPixelIndex = wavefrontIndices.top();
						wavefrontIndices.pop();
						int currentX = currentPixelIndex%width;
						int currentY = currentPixelIndex/width;
						++regionPixelTotal;
						unsigned short pixelValue = image(currentX, currentY);

						if (currentX < width - 1 && labels[currentPixelIndex + 1] == 0
							&& image(currentX + 1, currentY) != 0
							&& std::abs(pixelValue - image(currentX + 1, currentY)) <= maxDifference)
						{
							labels[currentPixelIndex + 1] = currentLabelIndex;
							wavefrontIndices.push(currentPixelIndex + 1);
						}

						if (currentX > 0 && labels[currentPixelIndex - 1] == 0
							&& image(currentX - 1, currentY) != 0
							&& std::abs(pixelValue - image(currentX - 1, currentY)) <= maxDifference)
						{
							labels[currentPixelIndex - 1] = currentLabelIndex;
							wavefrontIndices.push(currentPixelIndex - 1);
						}

						if (currentY < height - 1 && labels[currentPixelIndex + width] == 0
							&& image(currentX, currentY + 1) != 0
							&& std::abs(pixelValue - image(currentX, currentY + 1)) <= maxDifference)
						{
							labels[currentPixelIndex + width] = currentLabelIndex;
							wavefrontIndices.push(currentPixelIndex + width);
						}

						if (currentY > 0 && labels[currentPixelIndex - width] == 0
							&& image(currentX, currentY - 1) != 0
							&& std::abs(pixelValue - image(currentX, currentY - 1)) <= maxDifference)
						{
							labels[currentPixelIndex - width] = currentLabelIndex;
							wavefrontIndices.push(currentPixelIndex - width);
						}
					}

					if (regionPixelTotal <= maxSpeckleSize) {
						regionTypes[currentLabelIndex] = true;
						image(x, y) = 0;
					}
				}
			}
		}
	}
}

void SgmFlow::interpolateVZIndexImage(const rev::Image<unsigned short>& vzIndexImage,
									  rev::Image<unsigned short>& interpolatedVZIndexImage) const
{
	int width = vzIndexImage.width();
	int height = vzIndexImage.height();

	interpolatedVZIndexImage = vzIndexImage;
	for (int y = 0; y < height; ++y) {
		// initialize counter
		int count = 0;

		for (int x = 0; x < width; ++x) {
			if (interpolatedVZIndexImage(x, y) > 0) {   // if disparity is valid
				if (count >= 1) {   // at least one pixel requires interpolation
					// first and last value for interpolation
					int startX = x - count;
					int endX = x - 1;

					// set pixel to min disparity
					if (startX > 0 && endX < width-1) {
						unsigned short interpolationVZIndex = std::min(interpolatedVZIndexImage(startX - 1, y),
							interpolatedVZIndexImage(endX + 1, y));
						for (int interpolateX = startX; interpolateX <= endX; ++interpolateX) {
							interpolatedVZIndexImage(interpolateX, y) = interpolationVZIndex;
						}
					}
				}

				// reset counter
				count = 0;
			} else {   // otherwise increment counter
				++count;
			}
		}

		// extrapolate to the left
		for (int x = 0; x < width; ++x) {
			if (interpolatedVZIndexImage(x, y) > 0) {
				for (int interpolateX = 0; interpolateX < x; ++interpolateX) {
					interpolatedVZIndexImage(interpolateX, y) = interpolatedVZIndexImage(x, y);
				}
				break;
			}
		}

		// extrapolate to the right
		for (int x = width-1; x >= 0; --x) {
			if (interpolatedVZIndexImage(x, y) > 0) {
				for (int interpolateX = x+1; interpolateX < width; ++interpolateX) {
					interpolatedVZIndexImage(interpolateX, y) = interpolatedVZIndexImage(x, y);
				}
				break;
			}
		}
	}

	// for each column
	for (int x = 0; x < width; ++x) {
		// extrapolate to the top
		for (int y = 0; y < height; ++y) {
			if (interpolatedVZIndexImage(x, y) > 0) {
				for (int interpolateY = 0; interpolateY < y; ++interpolateY) {
					interpolatedVZIndexImage(x, interpolateY) = interpolatedVZIndexImage(x, y);
				}
				break;
			}
		}

		// extrapolate to the bottom
		for (int y = height-1; y >= 0; --y) {
			if (interpolatedVZIndexImage(x, y) > 0) {
				for (int interpolateY = y+1; interpolateY < height; ++interpolateY) {
					interpolatedVZIndexImage(x, interpolateY) = interpolatedVZIndexImage(x, y);
				}
				break;
			}
		}
	}
}
