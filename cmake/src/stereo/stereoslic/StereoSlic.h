#ifndef STEREO_SLIC_H
#define STEREO_SLIC_H

#include <vector>
#include <revlib.h>

class StereoSlic {
public:
    StereoSlic();
    
    void setEnergyType(const int energyType);
    void setIterationTotal(const int iterationTotal);
    void setWeightParameters(const double compactnessWeight,
                             const double disparityWeight,
                             const double noDisparityPenalty);
    
    void segment(const int superpixelTotal,
                 const rev::Image<unsigned char>& leftImage,
                 const rev::Image<unsigned char>& rightImage,
                 const rev::Image<unsigned short>& leftDisparityShortImage,
                 const rev::Image<unsigned short>& rightDisparityShortImage,
                 const double disparityFactor,
                 rev::Image<unsigned short>& segmentImage,
                 rev::Image<unsigned short>& segmentDisparityImage);
    
private:
    struct LabXYD {
        LabXYD() {
            color[0] = 0;  color[1] = 0;  color[2] = 0;
            position[0] = 0;  position[1] = 0;
            disparityPlane[0] = 0;  disparityPlane[1] = 0;  disparityPlane[2] = -1;
        }
        
        double color[3];
        double position[2];
        double disparityPlane[3];
    };
    struct DisparityPixel {
        double x;
        double y;
        double d;
    };
    
    void setColorAndDisparity(const rev::Image<unsigned char>& leftImage,
                              const rev::Image<unsigned char>& rightImage,
                              const rev::Image<unsigned short>& leftDisparityShortImage,
                              const rev::Image<unsigned short>& rightDisparityShortImage,
                              const double disparityFactor);
    void initializeSeeds(const int superpixelTotal);
    void performSegmentation();
    void enforceLabelConnectivity();
    void makeDisparityImage(const double disparityFactor,
                            rev::Image<unsigned short>& disparityImage);
    
    void checkInputImages(const rev::Image<unsigned char>& leftImage,
                          const rev::Image<unsigned char>& rightImage,
                          const rev::Image<unsigned short>& leftDisparityShortImage,
                          const rev::Image<unsigned short>& rightDisparityShortImage) const;
    void setStereoLabImages(const rev::Image<unsigned char>& leftImage,
                            const rev::Image<unsigned char>& rightImage);
    void calcLeftLabEdges();
    void setDisparityImages(const rev::Image<unsigned short>& leftDisparityShortImage,
                            const rev::Image<unsigned short>& rightDisparityShortImage,
                            const double disparityFactor);
    void setGridSeedPoint(const int superpixelTotal);
    void perturbSeeds();
    void assignLabel();
    void updateSeeds();
    void updateSeedsColorAndPosition();
    void estimateDisparityPlaneParameter();
    std::vector< std::vector<DisparityPixel> > makeDisparityPixelList() const;
    std::vector<double> estimateDisparityPlaneParameter(const std::vector<DisparityPixel>& disparityPixels) const;
    std::vector<double> estimateDisparityPlaneParameterRANSAC(const std::vector<DisparityPixel>& disparityPixels) const;
    void solvePlaneEquations(const double x1, const double y1, const double z1, const double d1,
                             const double x2, const double y2, const double z2, const double d2,
                             const double x3, const double y3, const double z3, const double d3,
                             std::vector<double>& planeParameter) const;
    void labelConnectedPixels(const int x,
                              const int y,
                              const int newLabelIndex,
                              std::vector<int>& newLabels,
                              std::vector<int>& connectedXs,
                              std::vector<int>& connectedYs) const;
    
    
    // Parameter
    int energyType_;
    int iterationTotal_;
    double gridSize_;
    double compactnessWeight_;
    double disparityWeight_;
    double noDisparityPenalty_;
    
    // Data
    int labelTotal_;
    std::vector<int> labels_;
    
    // Color and disparity images
    rev::Image<float> leftLabImage_;
    rev::Image<float> rightLabImage_;
    rev::Image<float> leftDisparityImage_;
    rev::Image<float> rightDisparityImage_;
    std::vector<double> leftLabEdges_;
    
    // Superpixel segments
    std::vector<LabXYD> seeds_;
    int stepSize_;
};

#endif
