#include "CameraMotion.h"
#include <fstream>
#include <eigen3/Eigen/Dense>

void CameraMotion::initialize(const std::string calibrationFilename, const std::string fundamentalMatrixFilename) {
	readCalibrationFile(calibrationFilename);
	readFundamentalMatrix(fundamentalMatrixFilename);

	calcRotationHomographyMatrix();
}

void CameraMotion::estimateRatioVzDisparity(std::vector<TriMatchedPoint>& matchedPoints) {
	computeWorldCoordinates(matchedPoints);

	const double pointDistanceThreshold = 15;
	std::vector<TriMatchedPoint> selectedMatchedPoints;
	selectMatchedPoint(matchedPoints, pointDistanceThreshold, selectedMatchedPoints);

	estimateRatioPlaneRANSAC(selectedMatchedPoints);
}

void CameraMotion::computeFirstEpipolarLine(const double secondX, const double secondY, Eigen::Vector3d& firstEpipolarLine) const {
	Eigen::Vector3d secondPoint;
	secondPoint << secondX, secondY, 1.0;

	firstEpipolarLine = fundamentalMatrix_.transpose()*secondPoint;
	double normalizeFactor = sqrt(firstEpipolarLine(0)*firstEpipolarLine(0) + firstEpipolarLine(1)*firstEpipolarLine(1));
	if (normalizeFactor > 1e-6) normalizeFactor = 1.0/normalizeFactor;
	else normalizeFactor = 1.0;
	firstEpipolarLine *= normalizeFactor;
}

void CameraMotion::computeSecondEpipolarLine(const double firstX, const double firstY, Eigen::Vector3d& secondEpipolarLine) const {
	Eigen::Vector3d firstPoint;
	firstPoint << firstX, firstY, 1.0;

	secondEpipolarLine = fundamentalMatrix_*firstPoint;
	double normalizeFactor = sqrt(secondEpipolarLine(0)*secondEpipolarLine(0) + secondEpipolarLine(1)*secondEpipolarLine(1));
	if (normalizeFactor > 1e-6) normalizeFactor = 1.0/normalizeFactor;
	else normalizeFactor = 1.0;
	secondEpipolarLine *= normalizeFactor;
}

void CameraMotion::computeFirstEpipole(double& firstEpipoleX, double& firstEpipoleY) const {
	Eigen::MatrixXd matF = fundamentalMatrix_;
	Eigen::JacobiSVD<Eigen::MatrixXd> svdMatF(matF, Eigen::ComputeFullV);

	firstEpipoleX = svdMatF.matrixV()(0, 2)/svdMatF.matrixV()(2, 2);
	firstEpipoleY = svdMatF.matrixV()(1, 2)/svdMatF.matrixV()(2, 2);
}

void CameraMotion::computeSecondEpipole(double& secondEpipoleX, double& secondEpipoleY) const {
	Eigen::MatrixXd matFt = fundamentalMatrix_.transpose();
	Eigen::JacobiSVD<Eigen::MatrixXd> svdMatFt(matFt, Eigen::ComputeFullV);

	secondEpipoleX = svdMatFt.matrixV()(0, 2)/svdMatFt.matrixV()(2, 2);
	secondEpipoleY = svdMatFt.matrixV()(1, 2)/svdMatFt.matrixV()(2, 2);
}


void CameraMotion::readCalibrationFile(const std::string calibrationFilename) {
	std::ifstream calibrationStream(calibrationFilename.c_str(), std::ios_base::in);
	if (calibrationStream.fail()) {
		throw rev::Exception("CameraMotion::setCalibrationParameters", "can't open file (" + calibrationFilename + ")");
	}

	double focalLength, cameraCenterX, cameraCenterY;
	double baselineMultipliedFocalLength;

	std::string tmpbuffer;
	double tmpvalue;
	calibrationStream >> tmpbuffer;
	calibrationStream >> focalLength;
	calibrationStream >> tmpvalue;
	calibrationStream >> cameraCenterX;
	calibrationStream >> tmpvalue;
	calibrationStream >> tmpvalue;
	calibrationStream >> tmpvalue;
	if (tmpvalue != focalLength) throw rev::Exception("CameraMotion::setCalibrationParameters", "focal length of x and y are different");
	calibrationStream >> cameraCenterY;
	calibrationStream >> tmpvalue;
	calibrationStream >> tmpvalue;
	calibrationStream >> tmpvalue;
	calibrationStream >> tmpvalue;
	calibrationStream >> tmpvalue;

	calibrationStream >> tmpbuffer;
	calibrationStream >> tmpvalue;
	if (tmpvalue != focalLength) throw rev::Exception("CameraMotion::setCalibrationParameters", "focal length of x and y are different");
	calibrationStream >> tmpvalue;
	calibrationStream >> tmpvalue;
	if (tmpvalue != cameraCenterX) throw rev::Exception("CameraMotion::setCalibrationParameters", "camera centers are different between two cameras");
	calibrationStream >> baselineMultipliedFocalLength;
	calibrationStream >> tmpvalue;
	calibrationStream >> tmpvalue;
	if (tmpvalue != focalLength) throw rev::Exception("CameraMotion::setCalibrationParameters", "focal length of x and y are different");
	calibrationStream >> tmpvalue;
	if (tmpvalue != cameraCenterY) throw rev::Exception("CameraMotion::setCalibrationParameters", "camera centers are different between two cameras");
	calibrationStream.close();

	calibrationMatrix_ << focalLength, 0, cameraCenterX,
		0, focalLength, cameraCenterY,
		0, 0, 1;
	stereoBaseline_ = baselineMultipliedFocalLength/focalLength;
}

void CameraMotion::readFundamentalMatrix(const std::string fundamentalMatrixFilename) {
	std::ifstream fundamentalFileStream(fundamentalMatrixFilename.c_str(), std::ios_base::in | std::ios_base::binary);
	if (fundamentalFileStream.fail()) {
		std::stringstream errorMessage;
		errorMessage << "can't open file (" << fundamentalMatrixFilename << ")";
		throw rev::Exception("FundamentalMatrix::readFundamentalMatrixFile", errorMessage.str());
	}
	double elements[9];
	int directionFlag;
	fundamentalFileStream.read(reinterpret_cast<char*>(elements), 9*sizeof(double));
	fundamentalFileStream.read(reinterpret_cast<char*>(&directionFlag), sizeof(int));
	fundamentalFileStream.close();

	fundamentalMatrix_ << elements[0], elements[1], elements[2],
		elements[3], elements[4], elements[5],
		elements[6], elements[7], elements[8];
	epipolarSearchDirectionFlag_ = directionFlag;
}

void CameraMotion::calcRotationHomographyMatrix() {
	Eigen::Matrix3d essentialMatrix = calibrationMatrix_.transpose()*fundamentalMatrix_*calibrationMatrix_;
	Eigen::JacobiSVD<Eigen::Matrix3d> svdEssential(essentialMatrix, Eigen::ComputeFullU | Eigen::ComputeFullV);
	Eigen::Matrix3d matW;
	matW << 0, -1, 0,
		1, 0, 0,
		0, 0, 1;
	Eigen::Matrix3d firstRotationMatrix = svdEssential.matrixU()*matW*svdEssential.matrixV().transpose();
	Eigen::Matrix3d secondRotationMatrix = svdEssential.matrixU()*matW.transpose()*svdEssential.matrixV().transpose();
	double rotationDeterminant = firstRotationMatrix.determinant();
	if (rotationDeterminant < 0) {
		firstRotationMatrix *= -1.0;
		secondRotationMatrix *= -1.0;
	}
	Eigen::Matrix3d rotationMatrix;
	if (firstRotationMatrix(0, 0) > 0 && firstRotationMatrix(1, 1) > 0 && firstRotationMatrix(2, 2) > 0) {
		rotationMatrix = firstRotationMatrix;
	} else {
		rotationMatrix = secondRotationMatrix;
	}

	rotationHomographyMatrix_ = calibrationMatrix_*rotationMatrix*calibrationMatrix_.inverse();
}

void CameraMotion::computeWorldCoordinates(std::vector<TriMatchedPoint>& matchedPoints) const {
	const double epsilon = 1e-6;

	double focalLength = calibrationMatrix_(0, 0);
	double cameraCenterX = calibrationMatrix_(0, 2);
	double cameraCenterY = calibrationMatrix_(1, 2);
	double coefficientDistance = focalLength*(-stereoBaseline_);

	int pointTotal = static_cast<int>(matchedPoints.size());
	for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
		double disparity = matchedPoints[pointIndex].firstLeftX() - matchedPoints[pointIndex].firstRightX();
		if (disparity < epsilon) {
			matchedPoints[pointIndex].setWorldCoordinates(0, 0, -1);
		} else {
			double worldZ = coefficientDistance/disparity;
			double worldX = (matchedPoints[pointIndex].firstLeftX() - cameraCenterX)/focalLength*worldZ;
			double worldY = (matchedPoints[pointIndex].firstLeftY() - cameraCenterY)/focalLength*worldZ;

			matchedPoints[pointIndex].setWorldCoordinates(worldX, worldY, worldZ);
		}
	}
}

void CameraMotion::selectMatchedPoint(const std::vector<TriMatchedPoint>& matchedPoints,
									  const double pointDistanceThreshold,
									  std::vector<TriMatchedPoint>& selectedPoints) const
{
	const double pointDistanceThresholdSquare = pointDistanceThreshold*pointDistanceThreshold;

	selectedPoints.clear();

	int pointTotal = static_cast<int>(matchedPoints.size());
	for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
		double pointX = matchedPoints[pointIndex].firstLeftX();
		double pointY = matchedPoints[pointIndex].firstLeftY();

		bool addFlag = true;
		for (int selectedIndex = 0; selectedIndex < static_cast<int>(selectedPoints.size()); ++selectedIndex) {
			double selectedPointX = selectedPoints[selectedIndex].firstLeftX();
			double selectedPointY = selectedPoints[selectedIndex].firstLeftY();

			double pointDistance = (pointX - selectedPointX)*(pointX - selectedPointX)
				+ (pointY - selectedPointY)*(pointY - selectedPointY);

			if (pointDistance < pointDistanceThresholdSquare) {
				addFlag = false;
				break;
			}
		}
		if (addFlag) {
			selectedPoints.push_back(matchedPoints[pointIndex]);
		}
	}
}

void CameraMotion::estimateRatioPlaneRANSAC(const std::vector<TriMatchedPoint>& matchedPoints) {
	const double inlierThreshold = 1.0;
	double inlierThresholdSquare = inlierThreshold*inlierThreshold;

	double epipoleX, epipoleY;
	computeSecondEpipole(epipoleX, epipoleY);

	std::vector<double> estimatedRatios;
	computeRatioVzDisparity(matchedPoints, epipoleX, epipoleY, estimatedRatios);

	int pointTotal = static_cast<int>(matchedPoints.size());
	int bestInlierTotal = 0;
	double bestRatioConstant = 0;
	double bestRatioGradientX = 0;
	double bestRatioGradientY = 0;
	int samplingTotal = pointTotal;
	for (int samplingCount = 0; samplingCount < samplingTotal; ++samplingCount) {
		int drawIndices[3];
		drawIndices[0] = rand() % pointTotal;
		while (estimatedRatios[drawIndices[0]] == 0) drawIndices[0] = rand() % pointTotal;
		drawIndices[1] = rand() % pointTotal;
		while (estimatedRatios[drawIndices[1]] == 0 || drawIndices[1] == drawIndices[0]) drawIndices[1] = rand() % pointTotal;
		drawIndices[2] = rand() % pointTotal;
		while (estimatedRatios[drawIndices[2]] == 0 || drawIndices[2] == drawIndices[0] || drawIndices[2] == drawIndices[1]) drawIndices[2] = rand() % pointTotal;

		double x1 = matchedPoints[drawIndices[0]].firstLeftX();
		double y1 = matchedPoints[drawIndices[0]].firstLeftY();
		double alpha1 = estimatedRatios[drawIndices[0]];
		double x2 = matchedPoints[drawIndices[1]].firstLeftX();
		double y2 = matchedPoints[drawIndices[1]].firstLeftY();
		double alpha2 = estimatedRatios[drawIndices[1]];
		double x3 = matchedPoints[drawIndices[2]].firstLeftX();
		double y3 = matchedPoints[drawIndices[2]].firstLeftY();
		double alpha3 = estimatedRatios[drawIndices[2]];

		double candidateRatioGradientX = ((alpha1 - alpha2)*(y1 - y3) - (alpha1 - alpha3)*(y1 - y2))/((x1 - x2)*(y1 - y3) - (x1 - x3)*(y1 - y2));
		double candidateRatioGradientY = (alpha1 - alpha3 - candidateRatioGradientX*(x1 - x3))/(y1 - y3);
		double candidateRatioConstant = alpha1 - candidateRatioGradientX*x1 - candidateRatioGradientY*y1;

		int inlierCount = 0;
		for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
			double candidateRatio = candidateRatioConstant + candidateRatioGradientX*matchedPoints[pointIndex].firstLeftX() + candidateRatioGradientY*matchedPoints[pointIndex].firstLeftY();

			Eigen::Vector3d epipolarLine;
			computeSecondEpipolarLine(matchedPoints[pointIndex].firstLeftX(), matchedPoints[pointIndex].firstLeftY(), epipolarLine);

			double epipolarSecondX, epipolarSecondY;
			double epipolarFlag = computeProjectedSecondPointOnEpipolarLine(matchedPoints[pointIndex], epipolarLine, epipolarSecondX, epipolarSecondY);
			if (!epipolarFlag) continue;

			double noDisparityPointX, noDisparityPointY;
			double distanceFromEpipole;
			computeNoDisparityPoint(matchedPoints[pointIndex], epipolarLine, epipoleX, epipoleY, noDisparityPointX, noDisparityPointY, distanceFromEpipole);

			double epipolarDirectionX = -epipolarLine(1);
			double epipolarDirectionY = epipolarLine(0);
			decideSignOfEpipolarDirection(noDisparityPointX, epipoleX, epipolarDirectionX, epipolarDirectionY);

			double stereoDisparity = matchedPoints[pointIndex].firstLeftX() - matchedPoints[pointIndex].firstRightX();
			double estimatedVzRatio = candidateRatio*stereoDisparity;
			double estimatedEpipolarDisparity = distanceFromEpipole*estimatedVzRatio/(1.0 - estimatedVzRatio);

			double estimatedSecondX = noDisparityPointX + estimatedEpipolarDisparity*epipolarDirectionX;
			double estimatedSecondY = noDisparityPointY + estimatedEpipolarDisparity*epipolarDirectionY;

			double errorAlongEpipolarLine = (estimatedSecondX - epipolarSecondX)*(estimatedSecondX - epipolarSecondX)
				+ (estimatedSecondY - epipolarSecondY)*(estimatedSecondY - epipolarSecondY);

			if (errorAlongEpipolarLine < inlierThresholdSquare) ++inlierCount;
		}

		if (inlierCount > bestInlierTotal) {
			bestInlierTotal = inlierCount;
			bestRatioGradientX = candidateRatioGradientX;
			bestRatioGradientY = candidateRatioGradientY;
			bestRatioConstant = candidateRatioConstant;
		}
	}

	ratioVzDisparityConstant_ = bestRatioConstant;
	ratioVzDisparityGradientX_ = bestRatioGradientX;
	ratioVzDisparityGradientY_ = bestRatioGradientY;
}

void CameraMotion::computeRatioVzDisparity(const std::vector<TriMatchedPoint>& matchedPoints, const double epipoleX, const double epipoleY, std::vector<double>& estimatedRatios) const {
	int pointTotal = static_cast<int>(matchedPoints.size());

	estimatedRatios.resize(pointTotal);
	for (int pointIndex = 0; pointIndex < pointTotal; ++pointIndex) {
		double stereoDisparity = matchedPoints[pointIndex].firstLeftX() - matchedPoints[pointIndex].firstRightX();
		double vzRatio = computeVzRatio(matchedPoints[pointIndex], epipoleX, epipoleY);
		if (stereoDisparity <= 0.1 || vzRatio <= 0) {
			estimatedRatios[pointIndex] = 0;
		} else {
			estimatedRatios[pointIndex] = vzRatio/stereoDisparity;
		}
	}
}

double CameraMotion::computeVzRatio(const TriMatchedPoint& matchedPoint, const double epipoleX, const double epipoleY) const {
	Eigen::Vector3d epipolarLine;
	computeSecondEpipolarLine(matchedPoint.firstLeftX(), matchedPoint.firstLeftY(), epipolarLine);

	double epipolarSecondX, epipolarSecondY;
	bool epipolarFlag = computeProjectedSecondPointOnEpipolarLine(matchedPoint, epipolarLine, epipolarSecondX, epipolarSecondY);
	if (!epipolarFlag) return 0;

	double noDisparityPointX, noDisparityPointY;
	double distanceFromEpipole;
	computeNoDisparityPoint(matchedPoint, epipolarLine, epipoleX, epipoleY, noDisparityPointX, noDisparityPointY, distanceFromEpipole);

	double epipolarDisparity = sqrt((epipolarSecondX - noDisparityPointX)*(epipolarSecondX - noDisparityPointX)
		+ (epipolarSecondY - noDisparityPointY)*(epipolarSecondY - noDisparityPointY));
	double normalizedDisparity = epipolarDisparity/distanceFromEpipole;
	double vzRatio = normalizedDisparity/(1 + normalizedDisparity);

	return vzRatio;
}

bool CameraMotion::computeProjectedSecondPointOnEpipolarLine(const TriMatchedPoint& matchedPoint,
															 const Eigen::Vector3d& epipolarLine,
															 double& epipolarSecondX,
															 double& epipolarSecondY) const
{
	const double epipolarThreshold = 1.0;

	double secondX = matchedPoint.secondLeftX();
	double secondY = matchedPoint.secondLeftY();

	double coefficientToEpipolarLine = -epipolarLine(0)*secondX - epipolarLine(1)*secondY - epipolarLine(2);
	if (fabs(coefficientToEpipolarLine) > epipolarThreshold) {
		epipolarSecondX = -1;
		epipolarSecondY = -1;
		return false;
	} else {
		epipolarSecondX = secondX + epipolarLine(0)*coefficientToEpipolarLine;
		epipolarSecondY = secondY + epipolarLine(1)*coefficientToEpipolarLine;
		return true;
	}
}

void CameraMotion::computeNoDisparityPoint(const TriMatchedPoint& matchedPoint,
										   const Eigen::Vector3d& epipolarLine,
										   const double epipoleX,
										   const double epipoleY,
										   double& noDisparityPointX,
										   double& noDisparityPointY,
										   double& distanceFromEpipole) const
{
	double firstX = matchedPoint.firstLeftX();
	double firstY = matchedPoint.firstLeftY();

	Eigen::Vector3d firstPoint;
	firstPoint << firstX, firstY, 1;

	Eigen::Vector3d rotatedPoint = rotationHomographyMatrix_*firstPoint;
	rotatedPoint /= rotatedPoint(2);
	double coefficientToEpipolarLine = -epipolarLine(0)*rotatedPoint(0) - epipolarLine(1)*rotatedPoint(1) - epipolarLine(2);

	noDisparityPointX = rotatedPoint(0) + epipolarLine(0)*coefficientToEpipolarLine;
	noDisparityPointY = rotatedPoint(1) + epipolarLine(1)*coefficientToEpipolarLine;

	distanceFromEpipole = sqrt((noDisparityPointX - epipoleX)*(noDisparityPointX - epipoleX) + (noDisparityPointY - epipoleY)*(noDisparityPointY - epipoleY));
}

void CameraMotion::decideSignOfEpipolarDirection(const double noDisparityPointX, const double epipoleX, double& epipolarDirectionX, double& epipolarDirectionY) const {
	if (epipolarSearchDirectionFlag_ == 0) {
		if ((noDisparityPointX < epipoleX && epipolarDirectionX > 0)
			|| (noDisparityPointX > epipoleX && epipolarDirectionX < 0))
		{
			epipolarDirectionX *= -1.0;
			epipolarDirectionY *= -1.0;
		}
	} else {
		if ((noDisparityPointX < epipoleX && epipolarDirectionX < 0)
			|| (noDisparityPointX > epipoleX && epipolarDirectionX > 0))
		{
			epipolarDirectionX *= -1.0;
			epipolarDirectionY *= -1.0;
		}
	}
}


void CameraMotion::writeCameraMotionFile(const std::string cameraMotionFilename) const {
	std::ofstream outputStream(cameraMotionFilename.c_str(), std::ios_base::out | std::ios_base::binary);
	if (outputStream.fail()) {
		throw rev::Exception("CameraMotion::writeMotionParameters", "can't open file (" + cameraMotionFilename + ")");
	}
	double fundElements[9];
	fundElements[0] = fundamentalMatrix_(0, 0);
	fundElements[1] = fundamentalMatrix_(0, 1);
	fundElements[2] = fundamentalMatrix_(0, 2);
	fundElements[3] = fundamentalMatrix_(1, 0);
	fundElements[4] = fundamentalMatrix_(1, 1);
	fundElements[5] = fundamentalMatrix_(1, 2);
	fundElements[6] = fundamentalMatrix_(2, 0);
	fundElements[7] = fundamentalMatrix_(2, 1);
	fundElements[8] = fundamentalMatrix_(2, 2);
	outputStream.write(reinterpret_cast<char*>(fundElements), 9*sizeof(double));
	int directionFlag = epipolarSearchDirectionFlag_;
	outputStream.write(reinterpret_cast<char*>(&directionFlag), sizeof(int));

	double cameraParameters[7];
	cameraParameters[0] = ratioVzDisparityConstant_;
	cameraParameters[1] = ratioVzDisparityGradientX_;
	cameraParameters[2] = calibrationMatrix_(0, 0);
	cameraParameters[3] = calibrationMatrix_(0, 2);
	cameraParameters[4] = calibrationMatrix_(1, 2);
	cameraParameters[5] = stereoBaseline_;
	cameraParameters[6] = ratioVzDisparityGradientY_;
	outputStream.write(reinterpret_cast<char*>(cameraParameters), 8*sizeof(double));

	outputStream.close();
}

void CameraMotion::readCameraMotionFile(const std::string cameraMotionFilename) {
	std::ifstream motionStream(cameraMotionFilename.c_str(), std::ios_base::in | std::ios_base::binary);
	if (motionStream.fail()) {
		throw rev::Exception("CameraMotion::readMotionParameters", "can't open file (" + cameraMotionFilename + ")");
	}

	double fundElements[9];
	motionStream.read(reinterpret_cast<char*>(fundElements), 9*sizeof(double));
	int directionFlag;
	motionStream.read(reinterpret_cast<char*>(&directionFlag), sizeof(int));
	double cameraParameters[7];
	motionStream.read(reinterpret_cast<char*>(cameraParameters), 8*sizeof(double));
	motionStream.close();

	fundamentalMatrix_ << fundElements[0], fundElements[1], fundElements[2],
		fundElements[3], fundElements[4], fundElements[5],
		fundElements[6], fundElements[7], fundElements[8];
	epipolarSearchDirectionFlag_ = directionFlag;

	ratioVzDisparityConstant_ = cameraParameters[0];
	ratioVzDisparityGradientX_ = cameraParameters[1];
	calibrationMatrix_ << cameraParameters[2], 0, cameraParameters[3],
		0, cameraParameters[2], cameraParameters[4],
		0, 0, 1;
	stereoBaseline_ = cameraParameters[5];
	ratioVzDisparityGradientY_ = cameraParameters[6];

	calcRotationHomographyMatrix();
}
