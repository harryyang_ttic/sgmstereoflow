#pragma once

#include <revlib.h>
#include "CameraMotion.h"

class CalibratedEpipolarFlowGeometry {
public:
	CalibratedEpipolarFlowGeometry(const int imageWidth, const int imageHeight, const CameraMotion& cameraMotion);

	double distanceFromEpipole(const int x, const int y) const { return distancesFromEpipole_(x, y); }
	double noDisparityPointX(const int x, const int y) const { return noDisparityPoints_(x, y, 0); }
	double noDisparityPointY(const int x, const int y) const { return noDisparityPoints_(x, y, 1); }
	double epipolarDirectionX(const int x, const int y) const { return epipolarDirectionVectors_(x, y, 0); }
	double epipolarDirectionY(const int x, const int y) const { return epipolarDirectionVectors_(x, y, 1); }

private:
	void buildTransformationImages(const int imageWidth, const int imageHeight, const CameraMotion& cameraMotion);

	rev::Image<double> distancesFromEpipole_;
	rev::Image<double> noDisparityPoints_;
	rev::Image<double> epipolarDirectionVectors_;
};
