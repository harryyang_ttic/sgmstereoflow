#pragma once

#include <revlib.h>

class CensusTransform {
public:
	CensusTransform();

	void setWindowRadius(const int windowRadius);
	void setWindowRadius(const int horizontalRadius, const int verticalRadius);

	void transform(const rev::Image<unsigned char>& grayscaleImage,
		rev::Image<int>& censusImage) const;

private:
	int horizontalRadius_;
	int verticalRadius_;
};
