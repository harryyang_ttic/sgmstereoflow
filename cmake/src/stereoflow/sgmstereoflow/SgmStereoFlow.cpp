#include "SgmStereoFlow.h"
#include <stack>
#include "StereoFlowGCCostCalculator.h"
#include "interpolateDisparityImage.h"

// Default parameters
const int SGMSTEREOFLOW_DEFAULT_DISPARITY_TOTAL = 256;
const int SGMSTEREOFLOW_DEFAULT_DATA_COST_TYPE = 2;
const int SGMSTEREOFLOW_DEFAULT_SOBEL_CAP_VALUE = 15;
const int SGMSTEREOFLOW_DEFAULT_CENSUS_WINDOW_RADIUS = 2;
const double SGMSTEREOFLOW_DEFAULT_CENSUS_WEIGHT_FACTOR = 1.0/6.0;
const int SGMSTEREOFLOW_DEFAULT_AGGREGATION_WINDOW_RADIUS = 2;
const int SGMSTEREOFLOW_DEFAULT_SMOOTHNESS_PENALTY_SMALL = 6*(SGMSTEREOFLOW_DEFAULT_AGGREGATION_WINDOW_RADIUS*2 + 1)*(SGMSTEREOFLOW_DEFAULT_AGGREGATION_WINDOW_RADIUS*2 + 1);
const int SGMSTEREOFLOW_DEFAULT_SMOOTHNESS_PENALTY_LARGE = 64*(SGMSTEREOFLOW_DEFAULT_AGGREGATION_WINDOW_RADIUS*2 + 1)*(SGMSTEREOFLOW_DEFAULT_AGGREGATION_WINDOW_RADIUS*2 + 1);
const bool SGMSTEREOFLOW_DEFAULT_PATH_EIGHT_DIRECTIONS = false;
const int SGMSTEREOFLOW_DEFAULT_CONSISTENCY_THRESHOLD = 1;

const double outputDisparityFactor = 256;

SgmStereoFlow::SgmStereoFlow() : disparityTotal_(SGMSTEREOFLOW_DEFAULT_DISPARITY_TOTAL),
	dataCostType_(SGMSTEREOFLOW_DEFAULT_DATA_COST_TYPE),
	sobelCapValue_(SGMSTEREOFLOW_DEFAULT_SOBEL_CAP_VALUE),
	censusWindowRadius_(SGMSTEREOFLOW_DEFAULT_CENSUS_WINDOW_RADIUS),
	censusWeightFactor_(SGMSTEREOFLOW_DEFAULT_CENSUS_WEIGHT_FACTOR),
	aggregationWindowRadius_(SGMSTEREOFLOW_DEFAULT_AGGREGATION_WINDOW_RADIUS),
	smoothnessPenaltySmall_(SGMSTEREOFLOW_DEFAULT_SMOOTHNESS_PENALTY_SMALL),
	smoothnessPenaltyLarge_(SGMSTEREOFLOW_DEFAULT_SMOOTHNESS_PENALTY_LARGE),
	pathEightDirections_(SGMSTEREOFLOW_DEFAULT_PATH_EIGHT_DIRECTIONS),
	consistencyThreshold_(SGMSTEREOFLOW_DEFAULT_CONSISTENCY_THRESHOLD) {}

void SgmStereoFlow::setDisparityTotal(const int disparityTotal) {
	if (disparityTotal <= 0 || disparityTotal%16 != 0) {
		throw rev::Exception("SgmStereoFlow::setDisparityTotal", "the number of disparities must be a multiple of 16");
	}

	disparityTotal_ = disparityTotal;
}

void SgmStereoFlow::setDataCostParameters(const int dataCostType,
									  const int sobelCapValue,
									  const int censusWindowRadius,
									  const int censusWeightFactor,
									  const int aggregationWindowRadius)
{
	dataCostType_ = dataCostType;
	sobelCapValue_ = sobelCapValue;
	censusWindowRadius_ = censusWindowRadius;
	censusWeightFactor_ = censusWeightFactor;
	aggregationWindowRadius_ = aggregationWindowRadius;
}

void SgmStereoFlow::setSmoothnessCostParameters(const int smoothnessPenaltySmall, const int smoothnessPenaltyLarge)
{
	if (smoothnessPenaltySmall < 0 || smoothnessPenaltyLarge < 0) {
		throw rev::Exception("SgmStereoFlow::setSmoothnessCostParameters", "smoothness penalty value is less than zero");
	}
	if (smoothnessPenaltySmall >= smoothnessPenaltyLarge) {
		throw rev::Exception("SgmStereoFlow::setSmoothnessCostParameters",
			"small value of smoothness penalty must be smaller than large penalty value");
	}

	smoothnessPenaltySmall_ = smoothnessPenaltySmall;
	smoothnessPenaltyLarge_ = smoothnessPenaltyLarge;
}

void SgmStereoFlow::setSgmParameters(const bool pathEightDirections, const int consistencyThreshold) {
	pathEightDirections_ = pathEightDirections;

	if (consistencyThreshold < 0) {
		throw rev::Exception("SgmStereoFlow::setSgmParameters", "Threshold for LR consistency must be positive");
	}
	consistencyThreshold_ = consistencyThreshold;
}

void SgmStereoFlow::compute(const rev::Image<unsigned char>& firstLeftImage,
							const rev::Image<unsigned char>& firstRightImage,
							const rev::Image<unsigned char>& secondLeftImage,
							const rev::Image<unsigned short>& stereoDisparityImage,
							const rev::Image<unsigned short>& flowVzIndexImage,
							const CameraMotion& cameraMotion,
							rev::Image<unsigned short>& leftDisparityImage,
							rev::Image<unsigned short>& firstFlowImage)
{
	// Image size
	int width = firstLeftImage.width();
	int height = firstLeftImage.height();

	// Epipolar flow geometry
	CalibratedEpipolarFlowGeometry epipolarFlowGeometry(width, height, cameraMotion);

	StereoFlowGCCostCalculator costCalculator;
	rev::Image<unsigned short> leftCostImage, rightCostImage;
	costCalculator.computeCostImage(firstLeftImage, firstRightImage, secondLeftImage, stereoDisparityImage, flowVzIndexImage, cameraMotion, epipolarFlowGeometry, leftCostImage, rightCostImage);

	// Semi-global matching
	allocateBuffer(leftCostImage.width(), leftCostImage.height());
	leftDisparityImage = performSGM(leftCostImage);
	rev::Image<unsigned short> rightDisparityImage;
	rightDisparityImage  = performSGM(rightCostImage);

	// Consistency check
	rev::Image<unsigned short> firstVzIndexImage, secondVzIndexImage;
	enforceLeftRightConsistency(leftDisparityImage, rightDisparityImage, secondVzIndexImage, stereoDisparityImage, flowVzIndexImage, cameraMotion, epipolarFlowGeometry);

	computeVzIndexFlowImage(leftDisparityImage, cameraMotion, epipolarFlowGeometry, firstVzIndexImage, secondVzIndexImage, firstFlowImage);
}

void SgmStereoFlow::allocateBuffer(const int width, const int height) {
	setBufferSize(width, height);

	buffer_.resize(totalBufferSize_);
}

void SgmStereoFlow::makeIntensityDifferenceImage(const rev::Image<unsigned char>& leftImage) {
	int width = leftImage.width();
	int height = leftImage.height();

	intensityDifferenceImage_.resize(width, height, 4);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			int intensityAbsoluteDifference;
			// to right
			if (x > 0) intensityAbsoluteDifference = abs(leftImage(x, y) - leftImage(x - 1, y));
			else intensityAbsoluteDifference = 0;
			if (intensityAbsoluteDifference < 1) intensityAbsoluteDifference = 1;
			intensityDifferenceImage_(x, y, 0) = intensityAbsoluteDifference;
			// to left
			if (x < width - 1) intensityAbsoluteDifference = abs(leftImage(x, y) - leftImage(x + 1, y));
			else intensityAbsoluteDifference = 0;
			if (intensityAbsoluteDifference < 1) intensityAbsoluteDifference = 1;
			intensityDifferenceImage_(x, y, 1) = intensityAbsoluteDifference;
			// to down
			if (y > 0) intensityAbsoluteDifference = abs(leftImage(x, y) - leftImage(x, y - 1));
			else intensityAbsoluteDifference = 0;
			if (intensityAbsoluteDifference < 1) intensityAbsoluteDifference = 1;
			intensityDifferenceImage_(x, y, 2) = intensityAbsoluteDifference;
			// to top
			if (y < height - 1) intensityAbsoluteDifference = abs(leftImage(x, y) - leftImage(x, y + 1));
			else intensityAbsoluteDifference = 0;
			if (intensityAbsoluteDifference < 1) intensityAbsoluteDifference = 1;
			intensityDifferenceImage_(x, y, 3) = intensityAbsoluteDifference;
		}
	}
}

rev::Image<unsigned short> SgmStereoFlow::performSGM(const rev::Image<unsigned short>& costImage) {
	const short costMax = SHRT_MAX;

	int width = costImage.width();
	int height = costImage.height();

	unsigned short* costImageData = costImage.dataPointer();
	int widthStepCostImage = costImage.widthStep();

	rev::Image<unsigned short> disparityImage(width, height, 1);
	unsigned short* disparityImageData = disparityImage.dataPointer();
	int widthStepDisparityImage = disparityImage.widthStep();

	short* costSums = buffer_.pointer();
	memset(costSums, 0, costSumBufferSize_*sizeof(short));

	short** pathCosts = new short* [pathRowBufferTotal_];
	short** pathMinCosts = new short* [pathRowBufferTotal_];

	const int processPassTotal = 2;
	for (int processPassCount = 0; processPassCount < processPassTotal; ++processPassCount) {
		int startX, endX, stepX;
		int startY, endY, stepY;
		if (processPassCount == 0) {
			startX = 0; endX = width; stepX = 1;
			startY = 0; endY = height; stepY = 1;
		} else {
			startX = width - 1; endX = -1; stepX = -1;
			startY = height - 1; endY = -1; stepY = -1;
		}

		for (int i = 0; i < pathRowBufferTotal_; ++i) {
			pathCosts[i] = costSums + costSumBufferSize_ + pathCostBufferSize_*i + pathDisparitySize_ + 8;
			memset(pathCosts[i] - pathDisparitySize_ - 8, 0, pathCostBufferSize_*sizeof(short));
			pathMinCosts[i] = costSums + costSumBufferSize_ + pathCostBufferSize_*pathRowBufferTotal_
				+ pathMinCostBufferSize_*i + pathTotal_*2;
			memset(pathMinCosts[i] - pathTotal_, 0, pathMinCostBufferSize_*sizeof(short));
		}

		for (int y = startY; y != endY; y += stepY) {
			unsigned short* disparityRow = disparityImageData + widthStepDisparityImage*y;
			unsigned short* pixelCostRow = costImageData + widthStepCostImage*y;
			short* costSumRow = costSums + costSumBufferRowSize_*y;

			memset(pathCosts[0] - pathDisparitySize_ - 8, 0, pathDisparitySize_*sizeof(short));
			memset(pathCosts[0] + width*pathDisparitySize_ - 8, 0, pathDisparitySize_*sizeof(short));
			memset(pathMinCosts[0] - pathTotal_, 0, pathTotal_*sizeof(short));
			memset(pathMinCosts[0] + width*pathTotal_, 0, pathTotal_*sizeof(short));

			for (int x = startX; x != endX; x += stepX) {
				int pathMinX = x*pathTotal_;
				int pathX = pathMinX*disparitySize_;

				int previousPathMin0, previousPathMin1, previousPathMin2, previousPathMin3;
				previousPathMin0 = pathMinCosts[0][pathMinX - stepX*pathTotal_] + smoothnessPenaltyLarge_;
				previousPathMin2 = pathMinCosts[1][pathMinX + 2] + smoothnessPenaltyLarge_;

				short *previousPathCosts0, *previousPathCosts1, *previousPathCosts2, *previousPathCosts3;
				previousPathCosts0 = pathCosts[0] + pathX - stepX*pathDisparitySize_;
				previousPathCosts2 = pathCosts[1] + pathX + disparitySize_*2;

				previousPathCosts0[-1] = previousPathCosts0[disparityTotal_] = costMax;
				previousPathCosts2[-1] = previousPathCosts2[disparityTotal_] = costMax;

				if (pathEightDirections_) {
					previousPathMin1 = pathMinCosts[1][pathMinX - pathTotal_ + 1] + smoothnessPenaltyLarge_;
					previousPathMin3 = pathMinCosts[1][pathMinX + pathTotal_ + 3] + smoothnessPenaltyLarge_;

					previousPathCosts1 = pathCosts[1] + pathX - pathDisparitySize_ + disparitySize_;
					previousPathCosts3 = pathCosts[1] + pathX + pathDisparitySize_ + disparitySize_*3;

					previousPathCosts1[-1] = previousPathCosts1[disparityTotal_] = costMax;
					previousPathCosts3[-1] = previousPathCosts3[disparityTotal_] = costMax;
				}

				short* pathCostCurrent = pathCosts[0] + pathX;
				const unsigned short* pixelCostCurrent = pixelCostRow + disparityTotal_*x;
				short* costSumCurrent = costSumRow + disparityTotal_*x;

				__m128i regPenaltySmall = _mm_set1_epi16(static_cast<short>(smoothnessPenaltySmall_));

				__m128i regPathMin0, regPathMin1, regPathMin2, regPathMin3;
				regPathMin0 = _mm_set1_epi16(static_cast<short>(previousPathMin0));
				regPathMin2 = _mm_set1_epi16(static_cast<short>(previousPathMin2));
				if (pathEightDirections_) {
					regPathMin1 = _mm_set1_epi16(static_cast<short>(previousPathMin1));
					regPathMin3 = _mm_set1_epi16(static_cast<short>(previousPathMin3));
				}
				__m128i regNewPathMin = _mm_set1_epi16(costMax);

				for (int d = 0; d < disparityTotal_; d += 8) {
					__m128i regPixelCost = _mm_load_si128(reinterpret_cast<const __m128i*>(pixelCostCurrent + d));

					__m128i regPathCost0, regPathCost1, regPathCost2, regPathCost3;
					regPathCost0 = _mm_load_si128(reinterpret_cast<const __m128i*>(previousPathCosts0 + d));
					regPathCost2 = _mm_load_si128(reinterpret_cast<const __m128i*>(previousPathCosts2 + d));

					regPathCost0 = _mm_min_epi16(regPathCost0,
						_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts0 + d - 1)),
						regPenaltySmall));
					regPathCost0 = _mm_min_epi16(regPathCost0,
						_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts0 + d + 1)),
						regPenaltySmall));
					regPathCost2 = _mm_min_epi16(regPathCost2,
						_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts2 + d - 1)),
						regPenaltySmall));
					regPathCost2 = _mm_min_epi16(regPathCost2,
						_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts2 + d + 1)),
						regPenaltySmall));

					regPathCost0 = _mm_min_epi16(regPathCost0, regPathMin0);
					regPathCost0 = _mm_adds_epi16(_mm_subs_epi16(regPathCost0, regPathMin0), regPixelCost);
					regPathCost2 = _mm_min_epi16(regPathCost2, regPathMin2);
					regPathCost2 = _mm_adds_epi16(_mm_subs_epi16(regPathCost2, regPathMin2), regPixelCost);

					_mm_store_si128(reinterpret_cast<__m128i*>(pathCostCurrent + d), regPathCost0);
					_mm_store_si128(reinterpret_cast<__m128i*>(pathCostCurrent + d + disparitySize_*2), regPathCost2);

					if (pathEightDirections_) {
						regPathCost1 = _mm_load_si128(reinterpret_cast<const __m128i*>(previousPathCosts1 + d));
						regPathCost3 = _mm_load_si128(reinterpret_cast<const __m128i*>(previousPathCosts3 + d));

						regPathCost1 = _mm_min_epi16(regPathCost1,
							_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts1 + d - 1)),

							regPenaltySmall));
						regPathCost1 = _mm_min_epi16(regPathCost1,
							_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts1 + d + 1)),
							regPenaltySmall));
						regPathCost3 = _mm_min_epi16(regPathCost3,
							_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts3 + d - 1)),
							regPenaltySmall));
						regPathCost3 = _mm_min_epi16(regPathCost3,
							_mm_adds_epi16(_mm_loadu_si128(reinterpret_cast<const __m128i*>(previousPathCosts3 + d + 1)),
							regPenaltySmall));

						regPathCost1 = _mm_min_epi16(regPathCost1, regPathMin1);
						regPathCost1 = _mm_adds_epi16(_mm_subs_epi16(regPathCost1, regPathMin1), regPixelCost);
						regPathCost3 = _mm_min_epi16(regPathCost3, regPathMin3);
						regPathCost3 = _mm_adds_epi16(_mm_subs_epi16(regPathCost3, regPathMin3), regPixelCost);

						_mm_store_si128(reinterpret_cast<__m128i*>(pathCostCurrent + d + disparitySize_), regPathCost1);
						_mm_store_si128(reinterpret_cast<__m128i*>(pathCostCurrent + d + disparitySize_*3), regPathCost3);
					}

					__m128i regMin02 = _mm_min_epi16(_mm_unpacklo_epi16(regPathCost0, regPathCost2),
						_mm_unpackhi_epi16(regPathCost0, regPathCost2));
					if (pathEightDirections_) {
						__m128i regMin13 = _mm_min_epi16(_mm_unpacklo_epi16(regPathCost1, regPathCost3),
							_mm_unpackhi_epi16(regPathCost1, regPathCost3));
						regMin02 = _mm_min_epi16(_mm_unpacklo_epi16(regMin02, regMin13),
							_mm_unpackhi_epi16(regMin02, regMin13));
					} else {
						regMin02 = _mm_min_epi16(_mm_unpacklo_epi16(regMin02, regMin02),
							_mm_unpackhi_epi16(regMin02, regMin02));
					}
					regNewPathMin = _mm_min_epi16(regNewPathMin, regMin02);

					__m128i regCostSum = _mm_load_si128(reinterpret_cast<const __m128i*>(costSumCurrent + d));

					if (pathEightDirections_) {
						regPathCost0 = _mm_adds_epi16(regPathCost0, regPathCost1);
						regPathCost2 = _mm_adds_epi16(regPathCost2, regPathCost3);
					}
					regCostSum = _mm_adds_epi16(regCostSum, regPathCost0);
					regCostSum = _mm_adds_epi16(regCostSum, regPathCost2);

					_mm_store_si128(reinterpret_cast<__m128i*>(costSumCurrent + d), regCostSum);
				}

				regNewPathMin = _mm_min_epi16(regNewPathMin, _mm_srli_si128(regNewPathMin, 8));
				_mm_storel_epi64(reinterpret_cast<__m128i*>(&pathMinCosts[0][pathMinX]), regNewPathMin);
			}

			if (processPassCount == processPassTotal - 1) {
				for (int x = 0; x < width; ++x) {
					short* costSumCurrent = costSumRow + disparityTotal_*x;
					int bestSumCost = costSumCurrent[0];
					int bestDisparity = 0;
					for (int d = 1; d < disparityTotal_; ++d) {
						if (costSumCurrent[d] < bestSumCost) {
							bestSumCost = costSumCurrent[d];
							bestDisparity = d;
						}
					}

					if (bestDisparity > 0 && bestDisparity < disparityTotal_ - 1) {
						int centerCostValue = costSumCurrent[bestDisparity];
						int leftCostValue = costSumCurrent[bestDisparity - 1];
						int rightCostValue = costSumCurrent[bestDisparity + 1];
						if (rightCostValue < leftCostValue) {
							bestDisparity = static_cast<int>(bestDisparity*outputDisparityFactor
								+ static_cast<double>(rightCostValue - leftCostValue)/(centerCostValue - leftCostValue)/2.0*outputDisparityFactor + 0.5);
						} else {
							bestDisparity = static_cast<int>(bestDisparity*outputDisparityFactor
								+ static_cast<double>(rightCostValue - leftCostValue)/(centerCostValue - rightCostValue)/2.0*outputDisparityFactor + 0.5);
						}
					} else {
						bestDisparity *= outputDisparityFactor;
					}

					disparityRow[x] = static_cast<unsigned short>(bestDisparity);
				}
			}

			std::swap(pathCosts[0], pathCosts[1]);
			std::swap(pathMinCosts[0], pathMinCosts[1]);
		}
	}
	delete [] pathCosts;
	delete [] pathMinCosts;

	speckleFilter(100, 2*outputDisparityFactor, disparityImage);

	return disparityImage;
}

void SgmStereoFlow::enforceLeftRightConsistency(rev::Image<unsigned short>& leftDisparityImage,
											rev::Image<unsigned short>& rightDisparityImage,
											rev::Image<unsigned short>& secondLeftDisparityImage,
											const rev::Image<unsigned short>& stereoDisparityImage,
											const rev::Image<unsigned short>& flowVzIndexImage,
											const CameraMotion& cameraMotion,
											const CalibratedEpipolarFlowGeometry& epipolarGeometry) const
{
	int width = leftDisparityImage.width();
	int height = leftDisparityImage.height();

	// Check left disparity image
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			if (leftDisparityImage(x, y) == 0) continue;

			int leftDisparityValue = static_cast<int>(static_cast<double>(leftDisparityImage(x, y))/outputDisparityFactor + 0.5);
			if (x - leftDisparityValue < 0) {
				leftDisparityImage(x, y) = 0;
				continue;
			}

			if (flowVzIndexImage(x, y) == 0) {
				int rightDisparityValue = static_cast<int>(static_cast<double>(rightDisparityImage(x-leftDisparityValue, y))/outputDisparityFactor + 0.5);
				if (rightDisparityValue == 0 || abs(leftDisparityValue - rightDisparityValue) > consistencyThreshold_) {
					leftDisparityImage(x, y) = 0;
				}
			}

			int largerThreshold = 2;
			int rightDisparityValue = static_cast<int>(static_cast<double>(rightDisparityImage(x-leftDisparityValue, y))/outputDisparityFactor + 0.5);
			if (rightDisparityValue != 0 && leftDisparityValue - rightDisparityValue > largerThreshold) {
				leftDisparityImage(x, y) = 0;
			}
		}
	}

	// Check right disparity image
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			if (rightDisparityImage(x, y) == 0)  continue;

			int rightDisparityValue = static_cast<int>(static_cast<double>(rightDisparityImage(x, y))/outputDisparityFactor + 0.5);
			if (x + rightDisparityValue >= width) {
				rightDisparityImage(x, y) = 0;
				continue;
			}

			int leftDisparityValue = static_cast<int>(static_cast<double>(leftDisparityImage(x+rightDisparityValue, y))/outputDisparityFactor + 0.5);
			if (leftDisparityValue == 0 || abs(rightDisparityValue - leftDisparityValue) > consistencyThreshold_) {
				rightDisparityImage(x, y) = 0;
			}
		}
	}
}

void SgmStereoFlow::computeFlowImage(const rev::Image<unsigned short>& disparityImage,
									 const CameraMotion& cameraMotion,
									 const CalibratedEpipolarFlowGeometry& epipolarGeometry,
									 rev::Image<unsigned short>& flowImage) const
{
	int width = disparityImage.width();
	int height = disparityImage.height();

	rev::Image<unsigned short> interpolatedDisparityImage = interpolateDisparityImage(disparityImage);

	flowImage.resize(width, height, 3);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			if (interpolatedDisparityImage(x, y) == 0) {
				flowImage(x, y, 0) = 0;
				flowImage(x, y, 1) = 0;
				flowImage(x, y, 2) = 0;
				continue;
			}

			double stereoDisparity = interpolatedDisparityImage(x, y)/outputDisparityFactor;
			double vzRatio = cameraMotion.ratioVzDisparity(x, y)*stereoDisparity;
			double flowDisparity = epipolarGeometry.distanceFromEpipole(x, y)*vzRatio/(1 - vzRatio);
			double secondX = epipolarGeometry.noDisparityPointX(x, y) + epipolarGeometry.epipolarDirectionX(x, y)*flowDisparity;
			double secondY = epipolarGeometry.noDisparityPointY(x, y) + epipolarGeometry.epipolarDirectionY(x, y)*flowDisparity;

			double firstFlowX = secondX - x;
			double firstFlowY = secondY - y;

			flowImage(x, y, 0) = static_cast<unsigned short>(firstFlowX*64.0 + 32768.5);
			flowImage(x, y, 1) = static_cast<unsigned short>(firstFlowY*64.0 + 32768.5);
			flowImage(x, y, 2) = 1;
		}
	}
}

void SgmStereoFlow::computeVzIndexFlowImage(const rev::Image<unsigned short>& disparityImage,
											const CameraMotion& cameraMotion,
											const CalibratedEpipolarFlowGeometry& epipolarGeometry,
											rev::Image<unsigned short>& firstVzIndexImage,
											rev::Image<unsigned short>& secondVzIndexImage,
											rev::Image<unsigned short>& flowImage) const
{
	int width = disparityImage.width();
	int height = disparityImage.height();

	rev::Image<unsigned short> interpolatedDisparityImage = interpolateDisparityImage(disparityImage);

	firstVzIndexImage.resize(width, height, 1);
	flowImage.resize(width, height, 3);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			if (interpolatedDisparityImage(x, y) == 0) {
				firstVzIndexImage(x, y) = 0;
				flowImage(x, y, 0) = 0;
				flowImage(x, y, 1) = 0;
				flowImage(x, y, 2) = 0;
				continue;
			}

			double stereoDisparity = interpolatedDisparityImage(x, y)/outputDisparityFactor;
			double vzRatio = cameraMotion.ratioVzDisparity(x, y)*stereoDisparity;
			double flowDisparity = epipolarGeometry.distanceFromEpipole(x, y)*vzRatio/(1 - vzRatio);
			double secondX = epipolarGeometry.noDisparityPointX(x, y) + epipolarGeometry.epipolarDirectionX(x, y)*flowDisparity;
			double secondY = epipolarGeometry.noDisparityPointY(x, y) + epipolarGeometry.epipolarDirectionY(x, y)*flowDisparity;

			double firstFlowX = secondX - x;
			double firstFlowY = secondY - y;

			if (disparityImage(x, y) != 0) firstVzIndexImage(x, y) = static_cast<unsigned short>(stereoDisparity*256.0 + 0.5);

			flowImage(x, y, 0) = static_cast<unsigned short>(firstFlowX*64.0 + 32768.5);
			flowImage(x, y, 1) = static_cast<unsigned short>(firstFlowY*64.0 + 32768.5);
			flowImage(x, y, 2) = 1;
		}
	}

	convertVZIndexImageFromFirstToSecond(cameraMotion, epipolarGeometry, firstVzIndexImage, secondVzIndexImage);
}

void SgmStereoFlow::convertVZIndexImageFromFirstToSecond(const CameraMotion& cameraMotion,
														 const CalibratedEpipolarFlowGeometry& epipolarFlowGeometry,
														 const rev::Image<unsigned short>& firstVZIndexImage,
														 rev::Image<unsigned short>& secondVZIndexImage) const
{
	int width = firstVZIndexImage.width();
	int height = firstVZIndexImage.height();

	secondVZIndexImage.resize(width, height, 1);
	secondVZIndexImage.setTo(0);
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			if (firstVZIndexImage(x, y) == 0) continue;

			double stereoDisparity = firstVZIndexImage(x, y)/outputDisparityFactor;
			double vzRatio = cameraMotion.ratioVzDisparity(x, y)*stereoDisparity;
			double disparityValue = epipolarFlowGeometry.distanceFromEpipole(x, y)*vzRatio/(1 - vzRatio);
			int secondX = static_cast<int>(epipolarFlowGeometry.noDisparityPointX(x, y)
				+ epipolarFlowGeometry.epipolarDirectionX(x, y)*disparityValue + 0.5);
			int secondY = static_cast<int>(epipolarFlowGeometry.noDisparityPointY(x, y)
				+ epipolarFlowGeometry.epipolarDirectionY(x, y)*disparityValue + 0.5);

			int sx0 = static_cast<int>(floor(secondX));
			int sx1 = sx0 + 1;
			int sy0 = static_cast<int>(floor(secondY));
			int sy1 = sy0 + 1;

			if (sx0 >= 0 && sx0 < width && sy0 >= 0 && sy0 < height
				&& (secondVZIndexImage(sx0, sy0) == 0 || secondVZIndexImage(sx0, sy0) < firstVZIndexImage(x, y)))
			{
				secondVZIndexImage(sx0, sy0) = firstVZIndexImage(x, y);
			}
			if (sx1 >= 0 && sx1 < width && sy0 >= 0 && sy0 < height
				&& (secondVZIndexImage(sx1, sy0) == 0 || secondVZIndexImage(sx1, sy0) < firstVZIndexImage(x, y)))
			{
				secondVZIndexImage(sx1, sy0) = firstVZIndexImage(x, y);
			}
			if (sx0 >= 0 && sx0 < width && sy1 >= 0 && sy1 < height
				&& (secondVZIndexImage(sx0, sy1) == 0 || secondVZIndexImage(sx0, sy1) < firstVZIndexImage(x, y)))
			{
				secondVZIndexImage(sx0, sy1) = firstVZIndexImage(x, y);
			}
			if (sx1 >= 0 && sx1 < width && sy1 >= 0 && sy1 < height
				&& (secondVZIndexImage(sx1, sy1) == 0 || secondVZIndexImage(sx1, sy1) < firstVZIndexImage(x, y)))
			{
				secondVZIndexImage(sx1, sy1) = firstVZIndexImage(x, y);
			}
		}
	}
}

void SgmStereoFlow::setBufferSize(const int width, const int height) {
	pathRowBufferTotal_ = 2;
	disparitySize_ = disparityTotal_ + 16;
	pathTotal_ = 8;
	pathDisparitySize_ = pathTotal_*disparitySize_;

	costSumBufferRowSize_ = width*disparityTotal_;
	costSumBufferSize_ = costSumBufferRowSize_*height;
	pathMinCostBufferSize_ = (width + 2)*pathTotal_;
	pathCostBufferSize_ = pathMinCostBufferSize_*disparitySize_;
	totalBufferSize_ = (pathMinCostBufferSize_ + pathCostBufferSize_)*pathRowBufferTotal_ + costSumBufferSize_ + 16;
}

void SgmStereoFlow::speckleFilter(const int maxSpeckleSize, const int maxDifference,
							  rev::Image<unsigned short>& image) const
{
	int width = image.width();
	int height = image.height();

	std::vector<int> labels(width*height, 0);
	std::vector<bool> regionTypes(1);
	regionTypes[0] = false;

	int currentLabelIndex = 0;

	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			int pixelIndex = width*y + x;
			if (image(x, y) != 0) {
				if (labels[pixelIndex] > 0) {
					if (regionTypes[labels[pixelIndex]]) {
						image(x, y) = 0;
					}
				}else {
					std::stack<int> wavefrontIndices;
					wavefrontIndices.push(pixelIndex);
					++currentLabelIndex;
					regionTypes.push_back(false);
					int regionPixelTotal = 0;
					labels[pixelIndex] = currentLabelIndex;

					while (!wavefrontIndices.empty()) {
						int currentPixelIndex = wavefrontIndices.top();
						wavefrontIndices.pop();
						int currentX = currentPixelIndex%width;
						int currentY = currentPixelIndex/width;
						++regionPixelTotal;
						unsigned short pixelValue = image(currentX, currentY);

						if (currentX < width - 1 && labels[currentPixelIndex + 1] == 0
							&& image(currentX + 1, currentY) != 0
							&& std::abs(pixelValue - image(currentX + 1, currentY)) <= maxDifference)
						{
							labels[currentPixelIndex + 1] = currentLabelIndex;
							wavefrontIndices.push(currentPixelIndex + 1);
						}

						if (currentX > 0 && labels[currentPixelIndex - 1] == 0
							&& image(currentX - 1, currentY) != 0
							&& std::abs(pixelValue - image(currentX - 1, currentY)) <= maxDifference)
						{
							labels[currentPixelIndex - 1] = currentLabelIndex;
							wavefrontIndices.push(currentPixelIndex - 1);
						}

						if (currentY < height - 1 && labels[currentPixelIndex + width] == 0
							&& image(currentX, currentY + 1) != 0
							&& std::abs(pixelValue - image(currentX, currentY + 1)) <= maxDifference)
						{
							labels[currentPixelIndex + width] = currentLabelIndex;
							wavefrontIndices.push(currentPixelIndex + width);
						}

						if (currentY > 0 && labels[currentPixelIndex - width] == 0
							&& image(currentX, currentY - 1) != 0
							&& std::abs(pixelValue - image(currentX, currentY - 1)) <= maxDifference)
						{
							labels[currentPixelIndex - width] = currentLabelIndex;
							wavefrontIndices.push(currentPixelIndex - width);
						}
					}

					if (regionPixelTotal <= maxSpeckleSize) {
						regionTypes[currentLabelIndex] = true;
						image(x, y) = 0;
					}
				}
			}
		}
	}
}
