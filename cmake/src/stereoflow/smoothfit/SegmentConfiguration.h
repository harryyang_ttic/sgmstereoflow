#ifndef SEGMENTCONFIGURATION_H
#define SEGMENTCONFIGURATION_H

#include <string>
#include <vector>
#include <revlib.h>
#include "SupportPoint.h"
#include "Segment.h"

class SegmentConfiguration {
public:
    void build(const rev::Image<unsigned short>& segmentIndexImage,
               const std::vector<SupportPoint>& supportPoints,
               const int windowRadius,
               const bool associateBoundary);
    void buildWithoutSupportPoint(const rev::Image<unsigned short>& segmentIndexImage);
    void appendSupportPointToSegmentData(const std::vector<SupportPoint>& supportPoints,
                                         const int windowWidth,
                                         const bool associateBoundary);
	
	void set(const rev::Image<unsigned short>& segmentIndexImage,
			 const rev::Image<int>& boundaryIndexImage,
			 const std::vector<Segment>& segments,
			 const std::vector<SegmentBoundary>& boundaries,
			 const std::vector<SegmentJunction>& junctions,
			 const std::vector<SegmentCrossJunction>& crossJunctions);
    void setFromFile(const std::string& configurationFilename);
    void writeToFile(const std::string& outputFilename);
	
    int width() const { return segmentIndexImage_.width(); }
    int height() const { return segmentIndexImage_.height(); }
	rev::Image<unsigned short> segmentIndexImage() const { return segmentIndexImage_; }
	rev::Image<int> boundaryIndexImage() const { return boundaryIndexImage_; }
    int segmentTotal() const { return static_cast<int>(segments_.size()); }
    Segment segment(const int index) const;
    int boundaryTotal() const { return static_cast<int>(boundaries_.size()); }
    SegmentBoundary boundary(const int index) const;
    int junctionTotal() const { return static_cast<int>(junctions_.size()); }
    SegmentJunction junction(const int index) const;
    int crossJunctionTotal() const { return static_cast<int>(crossJunctions_.size()); }
    SegmentCrossJunction crossJunction(const int index) const;
    
    void setSegmentPointDisparity(const int segmentIndex, const int pointIndex, const double disparity);
    void setBoundaryPointDisparity(const int boundaryIndex, const int pointIndex, const double disparity);
    void setJunctionPointDisparity(const int junctionIndex, const int pointIndex, const double disparity);
    void setCrossJunctionPointDisparity(const int crossJunctionIndex, const int pointIndex, const double disparity);
    
private:
    void setSegmentIndexImage(const rev::Image<unsigned short>& segmentIndexImage);
    void buildSegmentRelationship();
    void setDisparityCandidateMax(const std::vector<SupportPoint>& supportPoints);
    void appendSupportPoint(const std::vector<SupportPoint>& supportPoints,
                            const int windowRadius,
                            const bool associateBoundary);
    
    int countSegmentTotal() const;
    bool isHorizontalBoundary(const int x, const int y) const;
    bool isVerticalBoundary(const int x, const int y) const;
    int appendBoundary(const int firstSegmentIndex, const int secondSegmentIndex);
	void setBoundaryIndexImageHorizontal(const int x, const int y, const int boundaryIndex, const int boundaryWidth = 2);
	void setBoundaryIndexImageVertical(const int x, const int y, const int boundaryIndex, const int boundaryWidth = 2);
    bool isJunction(const int x, const int y, std::vector<int>& junctionSegmentIndices) const;
    void appendJunction(const int x, const int y, const std::vector<int>& junctionSegmentIndices);
    void setBoundaryIndicesInSegment(const int segmentIndex);
    void setBoundaryIndicesInAllJunctions();
    void setBoundaryIndicesInAllCrossJunctions();
    std::vector<int> getSegmentIndicesWithinWindow(const int x, const int y, const int windowRadius) const;
    
    int supportPointDisparityCandidateMax_;
	rev::Image<unsigned short> segmentIndexImage_;
	rev::Image<int> boundaryIndexImage_;
    std::vector<Segment> segments_;
    std::vector<SegmentBoundary> boundaries_;
    std::vector<SegmentJunction> junctions_;
    std::vector<SegmentCrossJunction> crossJunctions_;
};

#endif
