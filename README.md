This project contains the sgmstereo code, and the CodeBlock project, and some helper batch scripts I wrote to run results for a single case as well multiple cases.

cmake: The source files.

codeblock: the codeblock project you can open with codeblock. Also you can build the project to generate binaries.

helper: 

1. runstereoflow is used to run a single case
usage: runstereoflow left_image_t0 right_image_t0 left_image_t1 calib.txt

2. stereoflowbatch is used to run multiple cases in same folder
usage: stereoflowbatch left_image_folder right_image_folder calibration_file start_id end_id